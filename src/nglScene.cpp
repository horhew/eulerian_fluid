#include <ngl/NGLInit.h>
#include <QMouseEvent>
#include <QGuiApplication>
#include <fstream>

#include <glm/glm.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#include <nglScene.h>


//**********************************************************************************
//********************************* NGLScene ***************************************
//**********************************************************************************

//----------------------------NGLScene::NGLScene()----------------------------------

NGLScene::NGLScene()
{
  // re-size the widget to that of the parent (in this case the GLFrame passed in on construction)
  setTitle("FLIP");
}

//----------------------------NGLScene::~NGLScene()----------------------------------

NGLScene::~NGLScene()
{
  std::cout<<"Shutting down NGL, removing VAO's and Shaders\n";

  // ideally we should have a function (e.g. NGLScene::shutdown() ) to delete VBOs
  glDeleteBuffers(1, &m_vbo);

  // TOASK: Do I need to delete program or does QOpenGLwindow take care of it?
  glUseProgram(0);
  glDeleteProgram(m_programID);
}

//----------------------------NGLScene::resizeGL()----------------------------------

void NGLScene::resizeGL(int _w, int _h)
{
  glViewport(0,0,_w,_h);
  update();
}

//----------------------------NGLScene::readShaderCode()----------------------------

std::string NGLScene::readShaderCode(const char* _filename)
{
  std::cout<<_filename<<std::endl;
  std::ifstream input(_filename);
  if(!input.good())
  {
    std::cerr<<"File \""<<_filename<<"\" failed to load."<<std::endl;
    // (optional) exits the program
    exit(EXIT_FAILURE);
  }

  // read file (by char) from beginning to end (end-of-range iterator)
  std::string shaderCode = std::string(std::istreambuf_iterator<char>(input),
                                       std::istreambuf_iterator<char>());

  // close file stream and return shader code
  input.close();
  return shaderCode;
}

//----------------------------NGLScene::checkStatus()-------------------------------

void NGLScene::checkStatus(const GLuint& _objID, std::string _objName, bool _isShader)
{
  // Get compile/link status from input object
  GLint status;
  if(_isShader) {
    std::cout<<"Compiling "<<_objName<<std::endl;
    glGetShaderiv(_objID, GL_COMPILE_STATUS, &status);
  }
  else
  {
    std::cout<<"Linking "<<_objName<<std::endl;
    glGetProgramiv(_objID, GL_LINK_STATUS, &status);
  }

  if(status != GL_TRUE)
  {
    // Get length of the error message and allocate a buffer of needed size
    GLint infoLogLength;
    if(_isShader) { glGetShaderiv(_objID, GL_INFO_LOG_LENGTH, &infoLogLength); }
    else          { glGetProgramiv(_objID, GL_INFO_LOG_LENGTH, &infoLogLength); }
    GLchar* buffer = new GLchar[infoLogLength];

    // Get error into our buffer
    GLsizei bufferSize; // int (used by OpenGL to output the 'true' error msg length)
    if(_isShader) { glGetShaderInfoLog(_objID, infoLogLength, &bufferSize, buffer); }
    else          { glGetProgramInfoLog(_objID, infoLogLength, &bufferSize, buffer); }

    // Output info
    std::cout<<buffer<<std::endl;

    // Don't forget to delete[] the allocated buffer
    delete[] buffer;
  }
  else
  {
    if(_isShader) { std::cout<<"Compilation of "<<_objName<<" was successful!"<<std::endl; }
    else { std::cout<<"Linking "<<_objName<<" was successful!"<<std::endl; }
  }
}

//----------------------------NGLScene::installShaders()-----------------------------

void NGLScene::installShaders()
{
  // Create program
  m_programID = glCreateProgram();

  // Create VSO (Vertex Shader Object)----------------------------
  m_vso = glCreateShader(GL_VERTEX_SHADER);

  // Convert to GL format
  std::string codeBuf = readShaderCode("src/vertexShaderCode.glsl");
  const char* source = codeBuf.c_str();
  glShaderSource(m_vso, 1, &source, NULL);

  // Compile the shader, test for compilation errors
  glCompileShader(m_vso);
  checkStatus(m_vso, "vertexID");

  // Create FSO (Fragment Shader Object)--------------------------
  m_fso = glCreateShader(GL_FRAGMENT_SHADER);

  // Convert to GL format
  codeBuf = readShaderCode("src/fragmentShaderCode.glsl");
  source = codeBuf.c_str();
  glShaderSource(m_fso, 1, &source, NULL);

  // Compile the shader, test for compilation errors
  glCompileShader(m_fso);
  checkStatus(m_fso, "fragmentID");

  // Attach VSO and FSO to the Program Object---------------------
  glAttachShader(m_programID, m_vso);
  glAttachShader(m_programID, m_fso);

  // Link and use the Program Object
  glLinkProgram(m_programID);
  checkStatus(m_programID, "m_programID", false); // check for linking errors
  glUseProgram(m_programID);

  // Delete shader objects (could also delete after glUseProgram)
  glDeleteShader(m_vso);
  glDeleteShader(m_fso);
}

//-----------------------NGLScene::createVbo()--------------------------------------
void NGLScene::createVbo(const ShapeData& _shape)
{
  // generate VBO
  glGenBuffers(1, &m_vbo);
  glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
  glBufferData(GL_ARRAY_BUFFER, // target
               _shape.vertexBufferSize() + _shape.indexBufferSize(), // size
               0, // data (we initialize to null)
               GL_DYNAMIC_DRAW); // data usage hint

  // copy data into the VBO
  // it doesn't matter if we bind the buffers to
  // GL_ARRAY_BUFFER or GL_ELEMENT_ARRAY_BUFFER binding points.
  // Only glDrawElements() cares about GL_ELEMENT_ARRAY_BUFFER and all we have to do
  // is bind the same buffer to both GL_ELEMENT_ARRAY_BUFFER and GL_ARRAY_BUFFER
  // NOTE: that's being done in createVaos()
  glBufferSubData(GL_ARRAY_BUFFER, // target
               0, //offset
               _shape.vertexBufferSize(), // size
               _shape.vertices().get()); // data
  glBufferSubData(GL_ARRAY_BUFFER, // target
               _shape.vertexBufferSize(), //offset
               _shape.indexBufferSize(), // size
               _shape.indices().get()); // data
}

//----------------------------NGLScene::createVaos()--------------------------------
void NGLScene::createVao(const ShapeData& _shape)
{
  // generate vertex array
  glGenVertexArrays(1, &m_vaos[_shape.name()]);

  // bind array (all buffer bindings will be associated with the active VAO)
  glBindVertexArray(m_vaos[_shape.name()]);

  // bind buffer
  glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vbo);

  // set up data for vertex attributes
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0); // position
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(GLfloat) * 3)); // color
  glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(GLfloat) * 6)); // normals

  // enable vertex attributes
  glEnableVertexAttribArray(0); // Enable the vertex attribute 0 (position)
  glEnableVertexAttribArray(1); // Enable the vertex attribute 1 (color)
  glEnableVertexAttribArray(2); // Enable the vertex attribute 2 (normals)

  // store byte offset for indices (for use in other places)
  m_indexOffset[_shape.name()] = _shape.vertexBufferSize();
}

//----------------------------NGLScene::draw()---------------------------------------
void NGLScene::draw(const ShapeData& _shape, // shape name
                    const glm::mat4& _mM, // model matrix
                    const glm::mat4& _vpM, // view*projection matrix
                    const GLenum _drawMethod, // draw method
                    bool _indexing) // use vertex indexing
{
  // create buffer and array objects
  createVbo(_shape);
  createVao(_shape);

  // calculate mvp matrix for the geometry
  glm::mat4 mvpM = _vpM * _mM;

  // send matrices to the shader
  glUniformMatrix4fv(m_uniformLocations["mvp"], 1, GL_FALSE, &mvpM[0][0]);
  glUniformMatrix4fv(m_uniformLocations["m"], 1, GL_FALSE, &_mM[0][0]);

  // check for indexing and render
  if(_indexing)
    glDrawElements(_drawMethod, _shape.numIndices(), GL_UNSIGNED_SHORT, (void*)m_indexOffset[_shape.name()]);
  else
    glDrawArrays(_drawMethod, 0, _shape.numVertices());

  // we're done drawing, so delete VBOs and VAOs
  glBindVertexArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
  glDeleteVertexArrays(1, &m_vaos[_shape.name()]);
  glDeleteBuffers(1, &m_vbo);

}

//----------------------------NGLScene::drawGrid()------------------------------------

void NGLScene::drawGrid(const Grid& _grid, const glm::mat4& _vpM)
{
  // get grid and cell size
  float cellSize = _grid.cellSize();

  // loop over all 3 axes and draw the required number of planes
  std::string currAxis = "X";
  glm::mat4 modelM;
  for(int axis = 0; axis < 3; ++axis)
  {
    // check which axis we're iterating over
    if(axis == 1) { currAxis = "Y"; }
    if(axis == 2) { currAxis = "Z"; }

    // name of the current plane to be drawn
    std::string name = "plane" + currAxis;

    // copy and draw plane along active axis
    for(int i = 0; i <= _grid.size(axis); ++i)
    {
      // set transform matrix
      if(currAxis == "X") modelM = glm::translate(glm::vec3(i * cellSize, 0.0f, 0.0f));
      if(currAxis == "Y") modelM = glm::translate(glm::vec3(0.0f, i * cellSize, 0));
      if(currAxis == "Z") modelM = glm::translate(glm::vec3(0.0f, 0.0f, i * cellSize));

      // draw
      draw(m_gridPlanes[name], modelM, _vpM, GL_LINES);
    }
  }
}

//----------------------------NGLScene::initializeGL()-------------------------------

void NGLScene::initializeGL()
{
  // initialize the NGL lib
  ngl::NGLInit::instance();

  // create grid
  int size = 10;
  float cellSize = 1.0f;
  float center = (size * cellSize) / 2.0f;
  m_grid.reset(new Grid(size, size, size, cellSize));
  m_gridPlanes = m_grid->oglGrid();

  // set up particles
  glm::vec3 emitterPos(center+cellSize*2, center+cellSize*2, center);
  int numParticles = 15000;
  glm::vec3 spread = m_grid->size()/6.0f * m_grid->cellSize();
  m_grid->createParticles(emitterPos, numParticles, spread);

  // install shaders
  installShaders();

  // get uniform locations from the program
  m_uniformLocations["mvp"] = glGetUniformLocation(m_programID, "modelToProjectionMatrix"); // mat4
  m_uniformLocations["ambientLight"] = glGetUniformLocation(m_programID, "ambientLight"); // vec3
  m_uniformLocations["lightPosWorld"] = glGetUniformLocation(m_programID, "lightPositionWorld"); // vec3
  // vec3 position for specular lighting
  m_uniformLocations["eyeV"] = glGetUniformLocation(m_programID, "eyePositionWorld"); // vec3
  // model transform for world-space lighting
  m_uniformLocations["m"] = glGetUniformLocation(m_programID, "modelToWorldMatrix"); // mat4

  // set background color
  bgColor(200,200,200);

  // glEnable
  glEnable(GL_DEPTH_TEST); // enable depth testing for drawing
  glEnable(GL_MULTISAMPLE); // enable multisampling for smoother drawing
  //glEnable(GL_CULL_FACE); // enable face culling
  //glFrontFace(GL_CW); // set ClockWise as front-facing
  //glCullFace(GL_FRONT); // default is GL_BACK
  //glViewport(0,0,width(),height()); // as re-size is not explicitly called we need to do this.

  // start timers
  m_grid->timer() = startTimer(20); // 40ms = 25fps, 33ms =~ 30fps, 20ms = 50fps
  m_grid->setFrameDuration(20 / 1000.0f);
}

//----------------------------NGLScene::paintGL()------------------------------------

void NGLScene::paintGL()
{
  // clear the screen and depth buffer
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glViewport(0,0,width(),height());
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  glPointSize(5);

  // build matrices
  glm::mat4 vM = m_camera.getWorldToViewMatrix(); // view M
  glm::mat4 pM = glm::perspective(glm::radians(60.0f), (float)width()/height(), 0.1f, 100.0f); // projection M
  glm::mat4 vpM = pM * vM; // view_projection M
  glm::mat4 identity_mM; // identity matrix

  // build ambient light
  glm::vec4 ambientLight(0.1f, 0.1f, 0.1f, 1.0f);
  glUniform4fv(m_uniformLocations["ambientLight"], 1, &ambientLight[0]);

  // build diffuse light
  glm::vec3 lightPositionWorld(0.0f, 3.0f, 0.0f);
  glUniform3fv(m_uniformLocations["lightPosWorld"], 1, &lightPositionWorld[0]);

  // eye position for specular lighting
  glm::vec3 eyePosition = m_camera.position();
  glUniform3fv(m_uniformLocations["eye"], 1, &eyePosition[0]);

  /**********************************************************************/
  /***************************DRAW***************************************/
  /**********************************************************************/
  // draw grid
  if(m_ogl_grid)
  {
    drawGrid(*m_grid, vpM);
  }

  // draw fluid cells
  if(m_ogl_fluid_cells)
  {
    draw(m_grid->oglFluidCells(), identity_mM, vpM, GL_LINES);
  }

  // draw particles
  if(m_ogl_particles)
  {
    draw(m_grid->emitter().oglParticles(), identity_mM, vpM, GL_POINTS, false);
    draw(m_grid->emitter().oglNewParticles(), identity_mM, vpM, GL_POINTS, false);
  }

  // draw cell wall velocities
  if(m_ogl_cell_wall_velocities)
  {
    for(const Cell& cell : m_grid->cells())
    {
      //if(cell.state() == Cell::air) {
        draw(cell.oglVelocityVectors(0.15f, true, true, true), identity_mM, vpM, GL_LINES, false);
      //}
    }
  }

  // draw velocity field
  if(m_ogl_velocity_field)
  {
    draw(m_grid->oglVelocityField(), identity_mM, vpM, GL_LINES, false);
  }

  // draw interpolated velocity field
  glm::vec3 start(0.0f);
  glm::vec3 end = start + glm::vec3(m_grid->size().x);
  if(m_ogl_interpolated_velocity)
  {
    draw(m_grid->oglInterpolatedVelocityField(start, end, 20, 0.2f), identity_mM, vpM, GL_LINES, false);
  }

  // draw specific cells
  //draw(m_grid->oglCell(8,1,6, glm::vec3(1,1,0)),identity_mM, vpM, GL_LINES);

}

//----------------------------NGLScene::setColor()----------------------------------

void NGLScene::bgColor(int _r, int _g, int _b, float _a)
{
  float slice = 1.0f/255;
  glClearColor(_r * slice, _g * slice, _b * slice, _a);
}

//----------------------------NGLScene::events--------------------------------------

void NGLScene::timerEvent(QTimerEvent* _event )
{
  if(_event->timerId() == m_grid->timer())
  {
    if(m_grid->isAnimating()) {
      m_grid->animate();

      // update the GLWindow and re-draw
      update();
    }
  }
}

//---------------------------------------------------------------------------

void NGLScene::mouseMoveEvent (QMouseEvent * _event)
{
  if(m_clicked)
  {
    // camera pan
    m_camera.mouseUpdate(glm::vec2(_event->x(), _event->y()));

    // update the GLWindow and re-draw
    update();
  }
}

//---------------------------------------------------------------------------

void NGLScene::mousePressEvent ( QMouseEvent * _event)
{
  if(_event->button() == Qt::LeftButton)
  {
    m_clicked = true;
  }
}

//---------------------------------------------------------------------------

void NGLScene::mouseReleaseEvent ( QMouseEvent * _event )
{
  if(_event->button() == Qt::LeftButton)
  {
    m_clicked = false;
  }
}

//---------------------------------------------------------------------------

void NGLScene::wheelEvent(QWheelEvent *_event)
{

}

//---------------------------------------------------------------------------

void NGLScene::keyPressEvent(QKeyEvent *_event)
{
  // this method is called every time the main window recives a key event.
  // we then switch on the key value and set the camera in the GLWindow
  switch (_event->key())
  {
  // escape key to quit
  case Qt::Key_Escape : QGuiApplication::exit(EXIT_SUCCESS); break;

  // camera movement
  case Qt::Key_W : m_camera.moveForward(); break;
  case Qt::Key_S : m_camera.moveBackward(); break;
  case Qt::Key_A : m_camera.strafeLeft(); break;
  case Qt::Key_D : m_camera.strafeRight(); break;
  case Qt::Key_R : m_camera.moveUp(); break;
  case Qt::Key_F : m_camera.moveDown(); break;
  case Qt::Key_N : std::cout<<m_camera.position().x
                         <<", "<<m_camera.position().y
                         <<", "<<m_camera.position().z
                         <<std::endl;
                   std::cout<<m_camera.viewDirection().x
                         <<", "<<m_camera.viewDirection().y
                         <<", "<<m_camera.viewDirection().z
                         <<std::endl;
                   break;

  // simulation control
  case Qt::Key_C : m_camera.reset(); break;
  case Qt::Key_X : m_grid->reset(); m_grid->startAnimating(); break;
  case Qt::Key_Z : m_grid->isAnimating() ? m_grid->stopAnimating() : m_grid->startAnimating(); break;
  case Qt::Key_V : if(!m_grid->isAnimating()) { m_grid->animate(); }; break;
  case Qt::Key_B : if(m_grid->isAnimating()) { m_grid->emitter().emitParticles(m_emitRate); }; break;

  // debug visualization
  case Qt::Key_1 : m_ogl_grid ? m_ogl_grid = false : m_ogl_grid = true; break;
  case Qt::Key_2 : m_ogl_velocity_field ? m_ogl_velocity_field = false : m_ogl_velocity_field = true; break;
  case Qt::Key_3 : m_ogl_fluid_cells ? m_ogl_fluid_cells = false : m_ogl_fluid_cells = true; break;
  case Qt::Key_4 : m_ogl_cell_wall_velocities ? m_ogl_cell_wall_velocities = false : m_ogl_cell_wall_velocities = true; break;
  case Qt::Key_5 : m_ogl_interpolated_velocity ? m_ogl_interpolated_velocity = false : m_ogl_interpolated_velocity = true; break;
  case Qt::Key_6 : m_ogl_particles ? m_ogl_particles = false : m_ogl_particles = true; break;

  // draw modes
  case Qt::Key_T : glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); break;
  case Qt::Key_G : glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); break;

  default : break;
  }

  // finally update the GLWindow and re-draw
  update();
}
