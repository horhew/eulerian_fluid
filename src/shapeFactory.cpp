#include <shapeFactory.h>
#include <cstring>
#include <iostream>
#include <assert.h>

#define NUM_ARRAY_ELEMENTS(a) sizeof(a) / sizeof(*a)

//**********************************************************************************
//******************************** ShapeFactory ************************************
//**********************************************************************************

//--------------------------ShapeFactory::makePlane()------------------------------

ShapeData ShapeFactory::makePlane(std::string _name, Plane _plane,
                                  float _width, float _height)
{

  // Brief: creates a quad on the XZ plane

  ShapeData shape;

  // 1. make plane vertices
  shape.setNumVertices(4);
  shape.liveVertices().reset(new Vertex[4]);
  shape.liveUvs().reset((new GLfloat[8]));

  // iterate over vertices, set position, normal and color
  /*
  0--3
  |  |
  |  |
  1--2
  */
  glm::vec3 pos;
  glm::vec3 normal;
  if(_plane == ShapeFactory::xz || _plane == ShapeFactory::zx)
  {
    pos = glm::vec3(0, 0, 0);
    shape.vertex(0).setPosition(pos);
    pos = glm::vec3(0, 0, _height);
    shape.vertex(1).setPosition(pos);
    pos = glm::vec3(_width, 0, _height);
    shape.vertex(2).setPosition(pos);
    pos = glm::vec3(_width, 0, 0);
    shape.vertex(3).setPosition(pos);

    normal = glm::vec3(0, 1, 0);
  }
  else if(_plane == ShapeFactory::xy || _plane == ShapeFactory::yx)
  {
    pos = glm::vec3(0, _height, 0);
    shape.vertex(0).setPosition(pos);
    pos = glm::vec3(0, 0, 0);
    shape.vertex(1).setPosition(pos);
    pos = glm::vec3(_width, 0, 0);
    shape.vertex(2).setPosition(pos);
    pos = glm::vec3(_width, _height, 0);
    shape.vertex(3).setPosition(pos);

    normal = glm::vec3(0, 0, 1);
  }
  else if(_plane == ShapeFactory::yz || _plane == ShapeFactory::zy)
  {
    pos = glm::vec3(0, _height, _width);
    shape.vertex(0).setPosition(pos);
    pos = glm::vec3(0, 0, _width);
    shape.vertex(1).setPosition(pos);
    pos = glm::vec3(0, 0, 0);
    shape.vertex(2).setPosition(pos);
    pos = glm::vec3(0, _height, 0);
    shape.vertex(3).setPosition(pos);

    normal = glm::vec3(1, 0, 0);
  }

  // set vertex color
  for(int i = 0; i < 4; ++i)
  {
    Vertex& thisVert = shape.vertex(i);
    thisVert.setColor(glm::vec3(0.5, 0.5, 0.5));
    //TODO: add uvs
  }

  // 2. make plane indices
  // 2 triangles per quad, 2 lines per triangle, 2 indices per line
  shape.setNumIndices(8);
  shape.liveIndices().reset(new GLushort[8]);

  /*
  A--D
  |  |
  |  |
  B--C
  */
  shape.index(0) = 0; // A
  shape.index(1) = 1; // B

  shape.index(2) = 1; // B
  shape.index(3) = 2; // C

  shape.index(4) = 2; // C
  shape.index(5) = 3; // D

  shape.index(6) = 3; // D
  shape.index(7) = 0; // A

  // type & dimensions
  //shape.setType("plane");

  // increment shape count
  m_shapeCount++;

  // set shape name and add its name to the stored std::vector<std::strings>
  shape.setName(_name);
  addShape(shape.name());

  return shape;
}

//--------------------------ShapeFactory::makeCell()------------------------------
ShapeData ShapeFactory::makeCell(std::string _name, float _size)
{
  ShapeData shape;
  float size = _size * 0.5;

  // vertex data
  Vertex vertices[] =
  {
      Vertex(-size, +size, +size,// 0
             +1.0f, +0.0f, +0.0f),// RGB

      Vertex(-size, -size, +size,// 1
             +0.0f, +1.0f, +0.0f),// RGB

      Vertex(+size, -size, +size,// 2
             +0.0f, +0.0f, +1.0f),// RGB

      Vertex(+size, +size, +size,// 3
             +1.0f, +1.0f, +1.0f),// RGB

      Vertex(-size, -size, -size,// 4
             +0.6f, +1.0f, +0.7f),// RGB

      Vertex(+size, -size, -size,// 5
             +0.6f, +0.4f, +0.8f),// RGB

      Vertex(+size, +size, -size,// 6
             +0.2f, +0.8f, +0.7f),// RGB

      Vertex(-size, +size, -size,// 7
             +0.2f, +0.7f, +1.0f),// RGB
  };

  // copy vertex data into shape
  shape.setNumVertices(NUM_ARRAY_ELEMENTS(vertices));
  shape.liveVertices().reset(new Vertex[NUM_ARRAY_ELEMENTS(vertices)]);

  // set vertex color and align cell corner, not centre, to origin
  glm::vec3 color(1,0,0);
  glm::vec3 posCorrection(size, size, size);
  for(int i = 0; i < NUM_ARRAY_ELEMENTS(vertices); ++i)
  {
    vertices[i].addPosition(posCorrection);
    shape.vertex(i) = vertices[i];
    shape.vertex(i).setColor(color);
  }

  // index data
  GLushort indices[] =
  {
      0,1,  1,2,  2,3,  3,0, // Z+ face
      7,4,  4,5,  5,6,  6,7, // Z- face
      0,7,  1,4,  2,5,  3,6  // connection lines in Z
  };

  // copy index data into shape
  shape.setNumIndices(NUM_ARRAY_ELEMENTS(indices));
  shape.liveIndices().reset(new GLushort[NUM_ARRAY_ELEMENTS(indices)]);

  for(int i = 0; i < NUM_ARRAY_ELEMENTS(indices); ++i)
  {
    shape.index(i)= indices[i];
  }

  // increment shape count
  m_shapeCount++;

  // set shape name and add its name to the stored std::vector<std::strings>
  shape.setName(_name);
  addShape(shape.name());

  return shape;
}

//--------------------------ShapeFactory::randomColor()---------------------------
glm::vec3 ShapeFactory::randomColor()
{
  // random values between 0 and 1 (RAND_MAX is a constant defined in <cstdlib>)
  return glm::vec3(
        rand() / (float)RAND_MAX,
        rand() / (float)RAND_MAX,
        rand() / (float)RAND_MAX
        );
}
