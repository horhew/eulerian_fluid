#include <emitter.h>
#include <time.h>
#include <grid.h>
#include <omp.h>

//----------------------------ctors-------------------------------------------------
Emitter::~Emitter()
{

}

Emitter::Emitter(Grid*            _grid,
                 glm::vec3        _position,
                 int              _numParticles,
                 const glm::vec3& _spread,
                 Type             _type)
{
  // store grid pointer
  m_grid = _grid;

  // set emitter parameters
  m_position = _position;
  m_numParticles = _numParticles;

  // create and store particles
  m_particles.resize(_numParticles);

  // store spread values
  m_spread = _spread;

  srand(2); // random seed
  for(int i = 0; i < _numParticles; ++i)
  {
    // create a random position vector
    float x = -1 + (float)rand() / (RAND_MAX/(1+1));
    float y = -1 + (float)rand() / (RAND_MAX/(1+1));
    float z = -1 + (float)rand() / (RAND_MAX/(1+1));

    // sphere
    /*while(x*x + y*y + z*z > 1)
    {
      x = -1 + (float)rand() / (RAND_MAX/(1+1));
      y = -1 + (float)rand() / (RAND_MAX/(1+1));
      z = -1 + (float)rand() / (RAND_MAX/(1+1));
    }*/
    //glm::vec3 ppColor((float)rand()/RAND_MAX, (float)rand()/RAND_MAX, (float)rand()/RAND_MAX);

    glm::vec3 particlePosition(m_position.x + x * _spread.x,
                               m_position.y + y * _spread.y,
                               m_position.z + z * _spread.z);
    glm::vec3 particleVelocity(0,0,0);

    m_particles[i] = Particle(particlePosition, particleVelocity, this);
  }

  // create integrator
  m_integratorType = _type;
  if(_type == euler) { m_integrator.reset(new Euler(_grid)); }
  if(_type == rk2)   { m_integrator.reset(new RK2(_grid)); }
  if(_type == rk3)   { m_integrator.reset(new RK3(_grid)); }
  if(_type == rk4)   { m_integrator.reset(new RK4(_grid)); }
}

//----------------------------Emitter::update()--------------------------------------
void Emitter::update(float _dt)
{
  glm::vec3 newPos;
  // old particles
  #pragma omp parallel for private(newPos)
  for(int i = 0; i < numParticles(); ++i)
  {
    Particle& particle = m_particles[i];

    glm::vec3 velocity = m_grid->interpolateVelocity(particle.position());
    newPos = m_integrator->integrate(particle.position(), velocity, _dt);
    particle.setPosition(newPos);
  }

  // new particles
  #pragma omp parallel for
  for(int i = 0; i < numNewParticles(); ++i)
  {
    Particle& particle = m_newParticles[i];

    glm::vec3 velocity = m_grid->interpolateVelocity(particle.position());
    particle.setPosition(m_integrator->integrate(particle.position(), velocity, _dt));
  }
}


//----------------------------Emitter::emitParticles()------------------------------
void Emitter::emitParticles(int _pps, const glm::vec3& _spread)
{
  // calculate the correct fraction of particles that should be emitted within one step
  int numNewParticles = _pps * m_grid->timestep();
  m_numNewParticles += numNewParticles;

  float x,y,z;
  srand(m_numNewParticles);
  for(int i = 0; i < numNewParticles; ++i)
  {
    // create a random position vector
    x = -1 + (float)rand() / (RAND_MAX/(1+1));
    y = -1 + (float)rand() / (RAND_MAX/(1+1));
    z = -1 + (float)rand() / (RAND_MAX/(1+1));

    glm::vec3 particlePosition(m_position.x + x * _spread.x,
                               m_position.y,
                               m_position.z + z * _spread.z);
    glm::vec3 particleVelocity(0,0,0);

    // store particles
    m_newParticles.push_back(Particle(particlePosition, particleVelocity, this));
  }
}

//----------------------------Emitter::reset()--------------------------------------
void Emitter::reset()
{
  // reset initial particles to their initial state
  #pragma omp parallel for
  for(int i = 0; i < numParticles(); ++i)
  {
    Particle& particle = m_particles[i];

    particle.setPosition(particle.initPosition());
    particle.setVelocity(particle.initVelocity());
    particle.setCurrentLife(0);
    particle.setActive();
  }

  // delete any added particles
  m_newParticles.clear();
  m_numNewParticles = 0;
}

//----------------------------Emitter::oglParticles()--------------------------------
ShapeData Emitter::oglParticles()
{
  ShapeData particles("particles", "particles", m_numParticles, m_numParticles);

  // create required number of vertices
  particles.liveVertices().reset(new Vertex[m_numParticles]);

  // TOADD: particle color

  // store vertex attributes on the output shape
  srand(2);
  for(int i = 0; i < m_numParticles; ++i)
  {
    glm::vec3 pPos = m_particles[i].position();
    glm::vec3 pColor((float)rand()/RAND_MAX, (float)rand()/RAND_MAX, (float)rand()/RAND_MAX);
    particles.vertex(i).setPosition(pPos);
    particles.vertex(i).setColor(pColor);
  }

  return particles;
}

//----------------------------Emitter::oglNewParticles()-----------------------------
ShapeData Emitter::oglNewParticles(const glm::vec3& _color, bool _randomColor)
{
  ShapeData newParticles("newParticles", "newParticles", m_numNewParticles, m_numNewParticles);

  // create required number of vertices
  newParticles.liveVertices().reset(new Vertex[m_numNewParticles]);

  // particle color
  glm::vec3 pColor = _color;

  // store vertex attributes on the output shape
  srand(4);
  for(int i = 0; i < m_numNewParticles; ++i)
  {
    glm::vec3 pPos = m_newParticles[i].position();
    if(_randomColor)
    {
      pColor.r = (float)rand()/RAND_MAX;
      pColor.g = (float)rand()/RAND_MAX;
      pColor.b = (float)rand()/RAND_MAX;
    }
    newParticles.vertex(i).setPosition(pPos);
    newParticles.vertex(i).setColor(pColor);
  }
  return newParticles;
}


