#include <camera.h>
#include <iostream>

Camera::Camera()
{
  m_viewDirection = glm::vec3(0.0f, 0.0f, -1.0f); // normalized vector (!)
  //m_viewDirection = glm::vec3(0.959561, -0.224949, -0.169197);
  //m_position = glm::vec3(-8.92297, 1.07537, 2.7662);
  m_position = glm::vec3(5, 5, 19);
  m_defaultPosition = m_position;
  m_up = glm::vec3(0.0f, 1.0f, 0.0f);
  m_rotationSpeed = 0.25f;
  m_movementSpeed = 0.1f;
  m_strafeDirection = glm::cross(m_viewDirection, m_up);
}

glm::mat4 Camera::getWorldToViewMatrix() const
{
  return glm::lookAt(m_position, m_position + m_viewDirection, m_up);
}

void Camera::mouseUpdate(const glm::vec2& _newMousePosition)
{
  // get mouse delta from last position
  glm::vec2 mouseDelta = (_newMousePosition - m_oldMousePosition);

  // fix camera jump (mouseDelta.length() returns # of components, glm::length() returns magnitude)
  if(glm::length(mouseDelta) > 50.0f)
  {
    m_oldMousePosition = _newMousePosition;
    return;
  }


  // update view direction
  m_strafeDirection = glm::cross(m_viewDirection, m_up); // rotation axis (pitch)
  m_viewDirection = glm::mat3(
                      glm::rotate(glm::radians(-mouseDelta.x * m_rotationSpeed), m_up) *
                      glm::rotate(glm::radians(-mouseDelta.y * m_rotationSpeed), m_strafeDirection)
                    ) * m_viewDirection;

  // IMPORTANT! Leave this variable declaration in, or display window slows down
  // to a crawl when tumbling (don't really know why... compiler error? makes no sense)
  float important_variable;

  // update stored mouse position

  m_oldMousePosition = _newMousePosition;
}


// CAMERA MOVEMENT
void Camera::moveForward()
{
  // addition of scalar times normalized direction vector
  m_position += m_movementSpeed * m_viewDirection;
}

void Camera::moveBackward()
{
  m_position -= m_movementSpeed * m_viewDirection;
}

void Camera::strafeLeft()
{
  // "negative" addition
  m_position += -m_movementSpeed * m_strafeDirection;
}

void Camera::strafeRight()
{
  // "positive" addition
  m_position += m_movementSpeed * m_strafeDirection;
}

void Camera::moveUp()
{
  // add up vector
  m_position += m_movementSpeed * m_up;
}

void Camera::moveDown()
{
  // subtract up vector
  m_position -= m_movementSpeed * m_up;
}

void Camera::reset()
{
  m_position = m_defaultPosition;
  m_viewDirection = glm::vec3(0.0f, 0.0f, -1.0f); // normalized vector (!)
  m_up = glm::vec3(0.0f, 1.0f, 0.0f);
  m_strafeDirection = glm::cross(m_viewDirection, m_up);
}

