#include <grid.h>
#include <math.h>
#include <iostream>
#include <glm/geometric.hpp>
#include <utils.h>
#include <omp.h>

#define NUM_ARRAY_ELEMENTS(a) sizeof(a) / sizeof(*a)

//**********************************************************************************
//********************************* Grid *******************************************
//**********************************************************************************

//-----------------------------------Grid ctors-------------------------------------

Grid::Grid()
{

}

Grid::Grid(int _x, int _y, int _z, float _cellSize)
{
  // set member variables
  m_nx  = _x;
  m_ny = _y;
  m_nz  = _z;
  m_cellSize = _cellSize;
  m_integrator.reset(new RK4(this));

  // create default emitter
  m_emitter.reset(new Emitter(this, // Grid*
                              glm::vec3(m_nx * m_cellSize / 2.0f, // position
                                        m_ny * m_cellSize / 2.0f,
                                        m_nz * m_cellSize / 2.0f),
                              0, // num particles
                              glm::vec3(), // spread
                              Emitter::rk4)); // integrator type

  // create velocities & pressures
  // Note: the (n+1)-th velocity gets set to 0 automatically, which is
  // exactly what we want for the boundary conditions
  m_xVelocities.resize((_x + 1) * _y * _z);
  m_yVelocities.resize((_y + 1) * _x * _z);
  m_zVelocities.resize((_z + 1) * _x * _y);
  m_pressures.resize(_x * _y * _z);

  m_xVelocities0.resize((_x + 1) * _y * _z);
  m_yVelocities0.resize((_y + 1) * _x * _z);
  m_zVelocities0.resize((_z + 1) * _x * _y);
  m_pressures0.resize(_x * _y * _z);

  // create cells
  m_cells.resize(_x * _y * _z);
  m_boundaryCellIndices.reserve(2 * ((_x*_y) + (_x*(_z-2)) + ((_y-2)*(_z-2))));

  // resize velocityIndices vector to the number of cells (6 indices per cell)
  m_velocityIndices.resize(m_cells.size(), std::vector<int>(6));

  // this is an offset value to position the cells. All of them need to be offset
  // by half their width, so that the grid lies completely in positive x,y,z
  // e.g. otherwise the first cell's center would be at (0,0,0), whereas we
  // want it to be at (m_cellWidth/2, m_cellWidth/2, m_cellWidth/2)
  float posOffset = m_cellSize / 2.0;

  // loop over cells to set stuff
  for(int z = 0; z < m_nz; ++z) {
    for(int y = 0; y < m_ny; ++y) {
      for(int x = 0; x < m_nx; ++x) {

        // calculate corresponding array index for the specified xyz and get cell
        int cIndex = cellIndex(x,y,z);
        Cell* currCell = &m_cells[cIndex];

        // store x,y,z coords that the cell has within the grid
        currCell->setCellCoords(x ,y, z);

        // store pointer to grid on the cell
        currCell->setGrid(this);

        // resize cell
        currCell->setSize(_cellSize);

        // store velocity indices corresponding to the current cell
        m_velocityIndices[cIndex] = velocityIndices(x,y,z);
        std::vector<int> velIndices = m_velocityIndices[cIndex];

        // set pressure, velocity of current cell and store pointers to them
        m_pressures[cIndex]          = 0.0; // pressure
        m_xVelocities[velIndices[0]] = 0.0f; // xMin
        m_yVelocities[velIndices[1]] = 0.0f; // yMin
        m_zVelocities[velIndices[2]] = 0.0f; // zMin

        currCell->setPressurePointer(&m_pressures[velIndices[0]]);
        currCell->setVelocityPointers(&m_xVelocities[velIndices[0]], // xMin
                                      &m_yVelocities[velIndices[1]], // yMin
                                      &m_zVelocities[velIndices[2]], // zMin

                                      &m_xVelocities[velIndices[3]], // xMax
                                      &m_yVelocities[velIndices[4]], // yMax
                                      &m_zVelocities[velIndices[5]]);// zMax

        // set cell position
        currCell->setPosition(glm::vec3(x * m_cellSize + posOffset,
                                        y * m_cellSize + posOffset,
                                        z * m_cellSize + posOffset));

        // store pointers to cell neighbours which are needed for trilinear interpolation
        currCell->setInterpNeighbours();

        // check for grid boundary cells and set them as such
        if(   x == 0 || x == m_nx - 1
           || y == 0 || y == m_ny - 1
           || z == 0 || z == m_nz - 1)
        {
          // set as solid boundary and store cell index into the cell boundary list
          currCell->setInitialState(Cell::CellState::solid);
          currCell->setState(Cell::CellState::solid);

          // store cell index for later use
          m_boundaryCellIndices.push_back(cIndex);
        }
      } // end for (x)
    } // end for (y)
  } // end for (z)

  // set cell neighbours
  for(Cell& cell : m_cells) { cell.setNeighbours(); }

  // set up the grid for first iteration
  updateCellStates(true);
  setBoundaryVelocities();
  computeMaxVelocity();

  // store initial state of the grid
  m_xVelocities0 = m_xVelocities;
  m_yVelocities0 = m_yVelocities;
  m_zVelocities0 = m_zVelocities;
  m_pressures0   = m_pressures;
  m_maxVelocity0 = m_maxVelocity;

} // end Grid::Grid()

Grid::~Grid()
{

}

//-----------------------------------Grid::animate()-------------------------------
void Grid::animate()
{

  // compute timestep
  computeTimestep();

  // update cell states
  updateCellStates();

  // advect velocity field
  //extrapolateVelocity();
  advect();

  // apply external forces (e.g. gravity)
  applyGravity();

  // pressure calculations
  project();

  // extrapolate and set boundaries
  extrapolateVelocity();
  setBoundaryVelocities();

  // determine the max velocity on the grid (for the next timestep calculation)
  computeMaxVelocity();

  // move particles
  moveParticles();

  // advance time
  advanceTime();
}

//-----------------------------------Grid::createParticles()-----------------------
void Grid::createParticles(glm::vec3& _emitterPos, int _numParticles, glm::vec3& _spread)
{
  /******** BRIEF **********
  Creates _numParticles particles starting at _emitterPos and spreading within
  a rectangular prism defined by the _spread diagonal vector.
  *************************/
  // type of integrator that is already initialized
  Emitter::Type iType = m_emitter->integratorType();

  // recreate the emitter with the specified number of particles
  m_emitter.reset(new Emitter(this, _emitterPos, _numParticles, _spread, iType));

  // update cell states for the first update iteration
  updateCellStates(true);
  setBoundaryVelocities();
  computeMaxVelocity();
}

//-----------------------------------Grid::setBoundaryVelocities()-----------------
void Grid::setBoundaryVelocities()
{
  /******** BRIEF **********
  Sets the velocities of the solid boundary cells.
  *************************/

  int cIndex;
  #pragma omp parallel
  {

  // zero perpendicular flow
  #pragma omp for collapse(2) private(cIndex)
  for(int j = 0; j < m_ny; ++j)
  {
    for(int k = 0; k < m_nz; ++k)
    {
      cIndex = cellIndex(0, j, k);
      cell(cIndex)->setU(0);
      cell(cIndex)->setUplus(0);

      cIndex = cellIndex(m_nx-1, j, k);
      cell(cIndex)->setU(0);
      cell(cIndex)->setUplus(0);
    }
  }

  #pragma omp for collapse(2) private(cIndex)
  for(int k = 0; k < m_nz; ++k)
  {
    for(int i = 0; i < m_nx; ++i)
    {
      cIndex = cellIndex(i, 0, k);
      cell(cIndex)->setV(0);
      cell(cIndex)->setVplus(0);

      cIndex = cellIndex(i, m_ny-1, k);
      cell(cIndex)->setV(0);
      cell(cIndex)->setVplus(0);
    }
  }

  #pragma omp for collapse(2) private(cIndex)
  for(int j = 0; j < m_ny; ++j)
  {
    for(int i = 0; i < m_nx; ++i)
    {
      cIndex = cellIndex(i, j, 0);
      cell(cIndex)->setW(0);
      cell(cIndex)->setWplus(0);

      cIndex = cellIndex(i, j, m_nz-1);
      cell(cIndex)->setW(0);
      cell(cIndex)->setWplus(0);
    }
  }

  // free-slip (zero tangential friction) or no-slip (full tangential friction)
  #pragma omp for
  for(int i = 0; i < m_nx-1; ++i)
  {
    for(int j = 0; j < m_ny; ++j)
    {
      cell(i, j, 0).setUplus(m_slip * cell(i, j, 1).uplus());
      cell(i, j, m_nz-1).setUplus(m_slip * cell(i, j, m_nz-2).uplus());
    }

    for(int k = 0; k < m_nz; ++k)
    {
      cell(i, 0, k).setUplus(m_slip * cell(i, 1, k).uplus());
      cell(i, m_ny-1, k).setUplus(m_slip * cell(i, m_ny-2, k).uplus());
    }
  }

  #pragma omp for
  for(int j = 0; j < m_ny-1; ++j)
  {
    for(int i = 0; i < m_nx; ++i)
    {
      cell(i, j, 0).setVplus(m_slip * cell(i, j, 1).vplus());
      cell(i, j, m_nz-1).setVplus(m_slip * cell(i, j, m_nz-2).vplus());
    }

    for(int k = 0; k < m_nz; ++k)
    {
      cell(0, j, k).setVplus(m_slip * cell(1, j, k).vplus());
      cell(m_nx-1, j, k).setVplus(m_slip * cell(m_nx-2, j, k).vplus());
    }
  }

  #pragma omp for
  for(int k = 0; k < m_nz-1; ++k)
  {
    for(int i = 0; i < m_nx; ++i)
    {
      cell(i, 0, k).setWplus(m_slip * cell(i, 1, k).wplus());
      cell(i, m_ny-1, k).setWplus(m_slip * cell(i, m_ny-2, k).wplus());
    }

    for(int j = 0; j < m_ny; ++j)
    {
      cell(0, j, k).setWplus(m_slip * cell(1, j, k).wplus());
      cell(m_nx-1, j, k).setWplus(m_slip * cell(m_nx-2, j, k).wplus());
    }
  }

  } // end #pragma parallel
}

//-----------------------------------Grid::setBoundaryCells()----------------------
void Grid::setBoundaryCells()
{
  /******** BRIEF **********
  Finds all solid boundary cells (we stored their indices on initialization),
  and sets their state to 'solid'.
  *************************/
  for(const int& cIndex : m_boundaryCellIndices)
  {
    cell(cIndex)->setState(Cell::solid);
  }
}

//-----------------------------------Grid::velocityIndices()-----------------------
std::vector<int> Grid::velocityIndices(int _x, int _y, int _z)
{
  /******** BRIEF **********
   Returns a vector of the 6 indices (xMin, yMin, zMin, xMax, yMax, zMax)
   of the "walls" that make up the cell (_x, _y, _z).
   To be used to index into the 3 velocity arrays.
   ************************/

  // calculate cell index
  int cIndex = cellIndex(_x, _y, _z);

  // create vector
  std::vector<int> indices(6);

  // account for the extra cell wall (compared to number of cells)
  int offset;

  // LEFT (xMin)
  // for every z++, there are m_ny extra walls; for every y++, there is 1 extra wall
  offset = (m_ny * _z) + _y;
  indices[0] = cIndex + offset;

  // RIGHT (xMax)
  // same as above, + 1
  offset = (m_ny * _z) + _y + 1;
  indices[3] = cIndex + offset;

  // BOTTOM (yMin)
  // for every z++, there are m_nx extra walls
  offset = (m_nx * _z);
  indices[1] = cIndex + offset;

  // TOP (yMax)
  // same as above + m_nx
  offset = (m_nx * _z) + m_nx;
  indices[4] = cIndex + offset;

  // BACK (zMin)
  // no offset
  indices[2] = cIndex;

  // FRONT (zMax)
  // there are (m_nx * m_ny) extra walls
  offset = m_nx * m_ny;
  indices[5] = cIndex + offset;

  return indices;
}

//-----------------------------------Grid::interpolateVelocity()-------------------
glm::vec3 Grid::interpolateVelocity(float _x, float _y, float _z)
{
  /******** BRIEF **********
  More complete description of trilinear interpolation here:
  http://paulbourke.net/miscellaneous/interpolation/
  http://cg.informatik.uni-freiburg.de/intern/seminar/gridFluids_fluid_flow_for_the_rest_of_us.pdf
  https://sites.google.com/site/adamvanner/tutorials/voxel-mac-grid-velocities
  *************************/
  glm::vec3 interpolatedVelocity;

  // normalize input coordinates (essentially transforms cells to unit cells).
  float xNorm = _x/m_cellSize;
  float yNorm = _y/m_cellSize;
  float zNorm = _z/m_cellSize;

  // perform interpolation for each axis (x,y,z)
  for(int index = 0; index < 3; ++index)
  {
    // we are using the xMin, yMin and zMin velocity values
    // (hence the +=2 step, which skips over the Max values stored for each cell)

    // subtract 0.5 (half a cell) to align normalized interpolation "cell" to min bounds
    float x = xNorm - 0.5f;
    float y = yNorm - 0.5f;
    float z = zNorm - 0.5f;

    // we don't need to align the cell in the direction we're interpolating along,
    // so we're adding 0.5 back in. We also set the active velocity array
    std::vector<float>* vel;
    if(index == 0) { x += 0.5; vel = &m_xVelocities; } // X
    if(index == 1) { y += 0.5; vel = &m_yVelocities; } // Y
    if(index == 2) { z += 0.5; vel = &m_zVelocities; } // Z

    // floor values to get the integer part, which we'll use to transform the
    // interpolation cell coordinates to the the origin (0,0,0),
    // as well as to index into the cell array
    int i = floor(x);
    int j = floor(y);
    int k = floor(z);

    // check if we don't overshoot array bounds and clamp (if necessary)
    if(i < 0) { i = 0; } else if(i >= m_nx) { i = m_nx - 1; }
    if(j < 0) { j = 0; } else if(j >= m_ny) { j = m_ny - 1; }
    if(k < 0) { k = 0; } else if(k >= m_nz) { k = m_nz - 1; }

    // cell at (i,j,k)
    Cell& cijk = cell(i,j,k);

    // compute interpolated value
    float weights[8] = {(1-(x-i)) * (1-(y-j)) * (1-(z-k)), // i,   j,   k
                        (   x-i ) * (1-(y-j)) * (1-(z-k)), // i+1, j,   k
                        (1-(x-i)) * (   y-j ) * (1-(z-k)), // i,   j+1, k
                        (   x-i ) * (   y-j ) * (1-(z-k)), // i+1, j+1, k
                        (1-(x-i)) * (1-(y-j)) * (   z-k ), // i,   j,   k+1
                        (   x-i ) * (1-(y-j)) * (   z-k ), // i+1, j,   k+1
                        (1-(x-i)) * (   y-j ) * (   z-k ), // i,   j+1, k+1
                        (   x-i ) * (   y-j ) * (   z-k )};// i+1, j+1, k+1

    float tWeight = 1.0f; // (explained below)
    float interp = 0;
    // self(bottom-left) & bottom-right
    //if(cijk.state() == Cell::liquid) {
      interp += weights[0] * (*vel)[m_velocityIndices[cellIndex(i  ,j  ,k  )][index]];
    //} else { tWeight -= weights[0]; }

    if(cijk.hasInterpNeighbour(0)) {
      interp += weights[1] * (*vel)[m_velocityIndices[cellIndex(i+1,j  ,k  )][index]];
    } else { tWeight -= weights[1]; }

    // top-left & top-right
    if(cijk.hasInterpNeighbour(1)) {
      interp += weights[2] * (*vel)[m_velocityIndices[cellIndex(i  ,j+1,k  )][index]];
    } else { tWeight -= weights[2]; }

    if(cijk.hasInterpNeighbour(2)) {
      interp += weights[3] * (*vel)[m_velocityIndices[cellIndex(i+1,j+1,k  )][index]];
    } else { tWeight -= weights[3]; }

    // front-left & front-right
    if(cijk.hasInterpNeighbour(3)) {
      interp += weights[4] * (*vel)[m_velocityIndices[cellIndex(i  ,j  ,k+1)][index]];
    } else { tWeight -= weights[4]; }

    if(cijk.hasInterpNeighbour(4)) {
      interp += weights[5] * (*vel)[m_velocityIndices[cellIndex(i+1,j  ,k+1)][index]];
    } else { tWeight -= weights[5]; }

    // front-top-left & front-top-right
    if(cijk.hasInterpNeighbour(5)) {
      interp += weights[6] * (*vel)[m_velocityIndices[cellIndex(i  ,j+1,k+1)][index]];
    } else { tWeight -= weights[6]; }

    if(cijk.hasInterpNeighbour(6)) {
      interp += weights[7] * (*vel)[m_velocityIndices[cellIndex(i+1,j+1,k+1)][index]];
    } else { tWeight -= weights[7]; }

    // e.g. if cell (i,j,k) has no neighbours (case in which tWeight == weights[0]),
    // dividing by tWeight means that interp gets 100% weighted towards velocity (i,j,k).
    // Example: interp-----> (velocity(i,j,k) * 0.1) / 0.1 = velocity(i,j,k) <-----final interp
    //                         weights[0]--------^      ^------------tWeight
    interp /= tWeight;

    // store interpolated value in our (u,v,w) vector
    interpolatedVelocity[index] = interp;
  }

  return interpolatedVelocity;
}

//-----------------------------------Grid::extrapolateVelocity()-------------------
void Grid::extrapolateVelocity()
{
  /******** BRIEF **********
  Extrapolates known velocities to the 'free surface'(air) domain around the fluid.
  This is needed whenever we interpolate velocity values (e.g. during
  advection) outside the liquid bounds (in the air).
  More info in Bridson, 2016: Section 3.2 (p.33), Section 4.8 (p.64), Section 5.5.4 (p.96)
  On page 65, Figure 4.6 gives pseudocode for this function.
  *************************/

  // util vars
  std::vector<Cell*> w; // "wavefront" cells

  // Initialize the first wavefront (0 = known, 1 = initial wavefront, INT_MAX = unknown)
  for(int i = 0; i < numCells(); ++i)
  {
    Cell* cell = &m_cells[i];

    // SOLID cells are marked as INT_MAX - 1, a value that will not be used in extrapolation
    if(cell->state() == Cell::solid)
    {
      cell->setWavefront(INT_MAX - 1);
    }

    // LIQUID cells have known velocities, so they get set to 0
    else if(cell->state() == Cell::liquid)
    {
      cell->setWavefront(0);
    }

    // AIR cells neighbouring any liquid cells are our first wavefront (1)
    else if(cell->neighbour(0)->state() == Cell::liquid ||
            cell->neighbour(1)->state() == Cell::liquid ||
            cell->neighbour(2)->state() == Cell::liquid ||
            cell->neighbour(3)->state() == Cell::liquid ||
            cell->neighbour(4)->state() == Cell::liquid ||
            cell->neighbour(5)->state() == Cell::liquid)
    {
      cell->setWavefront(1);
      w.push_back(cell);
    }
    // AIR cells not neighbouring any liquid cells are marked as unknown(INT_MAX)
    else
    {
      cell->setWavefront(INT_MAX);
    }
  }

  // Loop over the entire grid and extrapolate velocity
  int t = 0;
  int numValidNeighbours;
  while(t < w.size())
  {
    // cell in wavefront
    Cell* cell = w[t];
    //std::cout<<cell->i()<<", "<<cell->j()<<", "<<cell->k()<<": ";

    numValidNeighbours = 0; // reset number of neighbours with known velocities
    glm::vec3 vel; // total added velocity of the neighbours

    // loop over all the cell's neighbours and get the total velocity from
    // "valid" ones (where wavefront is smaller, thus where velocity is known)
    for(Cell* neighbour : cell->neighbours())
    {
      // if neighbour is in a smaller wavefront
      if(neighbour->wavefront() < cell->wavefront())
      {
        // add up velocity
        vel.x += neighbour->u();
        vel.y += neighbour->v();
        vel.z += neighbour->w();

        // increase number of valid neighbours
        numValidNeighbours++;
      }

      // if neighbour is marked as INT_MAX (velocity unknown), add it to the NEXT wavefront
      else if(neighbour->wavefront() == INT_MAX)
      {
        neighbour->setWavefront(cell->wavefront() + 1);
        w.push_back(neighbour);
      }
    } // end for neighbours

    /*
    Set cell velocity.
    We only want to update the wall velocity if it doesn't border an already
    updated cell (aka. neighbour on that side must be in an equal or higher wavefront)
    */

    // U
    if(cell->neighbour(0)->state() != Cell::liquid)
    {
      cell->setU(vel.x / numValidNeighbours);
    }

    // V
    if(cell->neighbour(1)->state() != Cell::liquid)
    {
      cell->setV(vel.y / numValidNeighbours);
    }

    // W
    if(cell->neighbour(2)->state() != Cell::liquid)
    {
      cell->setW(vel.z / numValidNeighbours);
    }

    if(cell->i() == 8 && cell->j() == 1 && cell->k() == 6)
    {
      /*std::cout<<"(8,1,6): ";
      printVec(vel/float(numValidNeighbours));
      printVec(cell->velocity());*/
    }

    t++;
  } // end while
}

//-----------------------------------Grid::cell()----------------------------------
Cell& Grid::cell(int _i, int _j, int _k)
{
  // TODO: check for bounds
  // return cell based on input coords - (0,0,0) is bottom-left cell
  return m_cells[_k * (m_nx * m_ny) + _j * (m_nx) + _i];
}

//-----------------------------------Grid::cellAtPosition()------------------------
Cell& Grid::cellAtPosition(const glm::vec3& _pos)
{
  /******** BRIEF **********
  Returns a reference to the cell containing the point _pos.
  *************************/

  // floor the coords (take into account the cell size)
  int i = int(_pos.x / m_cellSize);
  int j = int(_pos.y / m_cellSize);
  int k = int(_pos.z / m_cellSize);

  // calculate corresponding cell index (within the Grid-stored array)
  int index = cellIndex(i, j, k);

  return m_cells[index];
}

//-----------------------------------Grid::cellIndex()-----------------------------
int Grid::cellIndex(int _x, int _y, int _z) const
{
  // TODO: check for bounds
  // cell based on input coords - (0,0,0) is bottom-left cell
  return _z * (m_nx * m_ny) + _y * (m_nx) + _x;
}

//-----------------------------------Grid::size()----------------------------------
int Grid::size(int _axis) const
{
  if     (_axis == 0) { return m_nx; }
  else if(_axis == 1) { return m_ny; }
  else                { return m_nz;}
}

//-----------------------------------Grid::reset()---------------------------------
void Grid::reset()
{
  // reset particles
  m_emitter->reset();

  // reset velocity field and pressure
  m_xVelocities = m_xVelocities0;
  m_yVelocities = m_yVelocities0;
  m_zVelocities = m_zVelocities0;
  m_maxVelocity = m_maxVelocity0;
  m_pressures = m_pressures0;

  // restore old velocities stored on the cells, and other values
  for(Cell& cell : m_cells)
  {
    cell.setOldU(0);
    cell.setOldV(0);
    cell.setOldW(0);
    cell.setOldP(0);
    cell.setWavefront(INT_MAX);
  }

  // reset time
  m_time = 0.0f;
}

//-----------------------------------Grid::updateCellStates()----------------------
void Grid::updateCellStates(bool _changeInit)
{
  /******** BRIEF **********
  Loops over every particle, checks which cell contains it and sets that to 'liquid'.
  All other cells (except solid cells) are set to 'air'.
  *************************/

  // set all cells except solid cells to 'air'
  #pragma omp parallel for
  for(int i = 0; i < numCells(); ++i) {
    Cell& cell = m_cells[i];
    if(cell.state() != Cell::solid) { cell.setState(Cell::air); }
  }

  // loop over particles and update the state of the cells they are in
  #pragma omp parallel for
  for(int i = 0; i < m_emitter->numParticles(); ++i)
  {
    Particle& particle = m_emitter->particles()[i];
    //  set the cell to 'liquid' if it contains the particle
    cellAtPosition(particle.position()).setState(Cell::liquid);

    // also change cell initial state (only used in Grid initialization)
    if(_changeInit) { cellAtPosition(particle.position()).setInitialState(Cell::liquid); }
  }

  #pragma omp parallel for
  for(int i = 0; i < m_emitter->numNewParticles(); ++i)
  {
    Particle& particle = m_emitter->newParticles()[i];
    //  set the cell to 'liquid' if it contains the particle
    cellAtPosition(particle.position()).setState(Cell::liquid);
  }

  // store the cell's index within an (imaginary) liquid cell array
  // we will need this later
  int liq = 0;
  for(Cell& cell : m_cells)
  {
    if(cell.state() == Cell::liquid)
    {
      cell.setLiquidIndex(liq);
      liq++;
    }
  }
}

//-----------------------------------Grid::clearAirVelocity()----------------------
void Grid::clearAirVelocity()
{
  /******** BRIEF **********
  Loops over air cells and sets to 0 all velocity faces that do not border liquid cells
  *************************/

  for(Cell& cell : m_cells)
  {
    if(cell.state() == Cell::air)
    {
      if(cell.neighbour(0)->state() == Cell::air) {cell.setU(0);} // xMin
      if(cell.neighbour(1)->state() == Cell::air) {cell.setV(0);} // yMin
      if(cell.neighbour(2)->state() == Cell::air) {cell.setW(0);} // zMin
      if(cell.neighbour(3)->state() == Cell::air) {cell.setUplus(0);} // xMax
      if(cell.neighbour(4)->state() == Cell::air) {cell.setVplus(0);} // yMax
      if(cell.neighbour(5)->state() == Cell::air) {cell.setWplus(0);} // zMax
    }
  }
}

//-----------------------------------Grid::applyGravity()--------------------------
void Grid::applyGravity()
{
  /******** BRIEF **********
  Adds gravity contribution in the v direction of the grid's velocity field.

  ATTENTION(!):
  Solid cells are not considered, but modifications to their inner walls can occur.
  This function does not take care of zeroing out any velocities at the
  boundaries. setBoundaryVelocities() should be called before advection takes place.
  *************************/

  // loop over all cells
  #pragma omp parallel for
  for(int i = 0; i < numCells(); ++i)
  {
    Cell& c = m_cells[i];

    if(c.state() == Cell::liquid)
    {
      // update v (yMin) walls of the cell
      // forward euler is enough, since force is constant (Stam, 1999)
      c.setV(c.v() + m_gravity *m_dt);

      // if cell above liquid is air, update top (yMax) velocity as well
      if(c.neighbour(4)->state() == Cell::air) { c.setVplus(c.vplus() + m_gravity * m_dt); }

    } // end if
  } // end for
}

//-----------------------------------Grid::advect()--------------------------------
void Grid::advect()
{
  /******** BRIEF **********
  Advects the velocity field by itself. Implements the semi-lagrangian method
  described by Stam, 1999. Because we are using a staggered MAC grid, the routine
  must be run 3 times, for u, v and w components of each cell's velocity.
  *************************/

  // create a hypothetical particle
  glm::vec3 currPos;
  glm::vec3 oldPos;
  glm::vec3 vel;
  float halfCell = m_cellSize / 2.0f;

  // reverse the integration direction (we technically reverse the velocity field)
  m_integrator->setReverse(true);

  // run over all cells that are marked as fluid and get "old" velocities
  #pragma omp parallel for private(currPos, oldPos, vel)
  for(int i =0; i < numCells(); ++i)
  {
    Cell& cell = m_cells[i];
    if(cell.state() != Cell::solid)
    {
      // ----------------------------------u----------------------------------
      // "end" position of the particle is at the center of the minX cell face
      currPos = cell.position();
      currPos.x -= halfCell;

      // get velocity at the particle's "end" position
      vel = interpolateVelocity(currPos.x, currPos.y, currPos.z);

      // integrate particle backwards with that velocity
      oldPos = m_integrator->integrate(currPos, vel, m_dt);

      // get (u) velocity at the particle's "past" position,
      // then store that value on the cell.
      // we only update velocities when we're done looping.
      cell.setOldU(interpolateVelocity(oldPos).x);

      // ----------------------------------v----------------------------------
      // "end" position of the particle is at the center of the minX cell face
      currPos = cell.position();
      currPos.y -= halfCell;

      // get velocity at the particle's "end" position
      vel = interpolateVelocity(currPos.x, currPos.y, currPos.z);

      // integrate particle backwards with that velocity
      oldPos = m_integrator->integrate(currPos, vel, m_dt);

      // get (v) velocity at the particle's "past" position,
      // then store that value on the cell.
      // we only update velocities when we're done looping.
      cell.setOldV(interpolateVelocity(oldPos).y);

      // ----------------------------------w----------------------------------
      // "end" position of the particle is at the center of the minZ cell face
      currPos = cell.position();
      currPos.z -= halfCell;

      // get velocity at the particle's "end" position
      vel = interpolateVelocity(currPos.x, currPos.y, currPos.z);

      // integrate particle backwards with that velocity
      oldPos = m_integrator->integrate(currPos, vel, m_dt);

      // get (w) velocity at the particle's "past" position,
      // then store that value on the cell.
      // we only update velocities when we're done looping.
      cell.setOldW(interpolateVelocity(oldPos).z);
    }
  }

  // set "new" velocities from "old" velocities
  #pragma omp parallel for
  for(int i = 0; i < numCells(); ++i)
  {
    Cell& cell = m_cells[i];
    if(cell.state() != Cell::solid)
    {
      cell.setU(cell.oldu());
      cell.setV(cell.oldv());
      cell.setW(cell.oldw());
    }
  }
}

//-----------------------------------Grid::computeNegativeDivergence()-------------
void Grid::computeNegativeDivergence()
{
  /******** BRIEF **********
  Loops over all the liquid cells, computes and stores the NEGATIVE
  velocity divergence of the cell. It is approximating the divergence(aka gradient)
  using accurate central differences (Bridson, 2016, p.72, Eq.5.5 && Figure 5.3):

  div(velocity) = (u(i+1/2,j,k) - u(i-1/2,j,k))/dx + // (du/dx +
                  (v(i,j+1/2,k) - v(i,j-1/2,k))/dx + //   dv/dy +
                  (w(i,j,k+1/2) - w(i,j,k-1/2))/dx   //    dw/dz) = div(velocity)
  Note: because cells are perfect cubes, we use dx everywhere instead of dx,dy,dz.

  This method computes the rhs (right-hand side, or b) term of a linear eq. of the form:

                                  Ax = b, where

  A is a matrix containing pressure coefficients (computed in computePressureCoefficients()),
  x is a vector containing unknown pressure values that we need to solve for,
  b is a vector containing negative velocity divergences of the (liquid) cells.
  *************************/

  // resize the vector to the number of cells;
  m_b = Eigen::VectorXd(numLiquidCells());

  double scale = 1.0/m_cellSize;

  // loop over all liquid cells, store negative divergence for each cell
  #pragma omp parallel for
  for(int i = 0; i < numCells(); ++i)
  {
    Cell& cell = m_cells[i];
    if(cell.state() == Cell::liquid)
    {
      // negative velocity divergence of the cell, as described in the function BRIEF above
      m_b[cell.liquidIndex()] = -scale *((cell.uplus() - cell.u()) +
                                         (cell.vplus() - cell.v()) +
                                         (cell.wplus() - cell.w()));
    }
  }
}

//-----------------------------------Grid::computePressures()----------------------
void Grid::computePressures()
{
  /******** BRIEF **********
  Loops over all the liquid cells, computes and stores the pressure coefficients
  in a sparse matrix A. Then solves the linear system Ax = b using a linear solver.

  Bridson (2016) explains the pressure calculation in detail:
    Chapter 5.3, p.74-78
  Eigen sparse matrix tutorial:
    http://eigen.tuxfamily.org/dox/group__TutorialSparse.html#title8
  Eigen Conjugate Gradient info:
    http://eigen.tuxfamily.org/dox/classEigen_1_1ConjugateGradient.html
  *************************/

  // clear the vector where we store the Triplet(i,j,value) objects
  m_pressureCoefficients.clear();

  // coefficient by which all pressures are scaled
  double scale = m_dt / (m_density * m_cellSize * m_cellSize); // Δt/(ρ*Δx^2)

  // number of non-solid neighbours (used as coefficient for the pressure of the current cell)
  int numNeighbours = 0;

  // loop over all liquid cells, store matrix entries
  for(Cell& cell : m_cells)
  {    
    if(cell.state() == Cell::liquid)
    {
      // reset number of non-solid neighbours
      numNeighbours = 0;

      /******** MORE INFO **********
      Because we're only storing half the sparse matrix (the upper triangle), the idea is
      to add neighbour coefficients only by looking at the negative x,y,z (relative to current cell).
      In other words, we add the pressure coefficient of the current cell to its neighbour's row.
      This ensures that values are only added in the upper triangle of the matrix.

      The pressure equation is a Poisson problem: -Δt/ρ * ∇*∇p = -∇u, which translated to finite
      differences looks something like this (in 2D):

               / 4*p(i,j) - p(i+1,j) - p(i,j+1) - p(i-1,j) - p(i,j-1) \      / u(i+1/2,j) - u(i-1/2,j)   v(i,j+1/2) - v(i,j-1/2) \
      (Δt/ρ)* | _____________________________________________________  | = -|  _______________________ + _______________________  |
               \                       Δx^2                           /      \           Δx                         Δx           /

      NOTE: All p's have a common coefficient: -Δt/(ρ*Δx^2), a value we assign to the 'scale' variable above.
      NOTE: The number in front of p(i,j) represents how many non-solid cells are neighbouring the current cell.
            We store it as 'numNeighbours'.

      For every neighbouring LIQUID cell, we increase the coefficient multiplier of p(i,j): numNeigbours++
      ...and we also store a coeffcient for that cell: Triplet(i,j, value)
      For every neigbouring AIR cell, we increase the coefficient multiplier of p(i,j): numNeighbours++
      ...but we don't store any coefficient for the neighbour. E.g. if cell(i-1,j) is AIR, it means p(i-1,j) has a coeff. of 0
      For every neigbouring SOLID cell, we decrease the coefficient multiplier of p(i,j): in our case this translates
      ...to NOT incrementing numNeighbours or doing anything else.

      Bridson (2016) explains the pressure equations in detail: Chapter 5.3, p.74-78. I recommend reading
      the entire chapter 5 in order to understand what is happening.
      *****************************/

      // test for neighbours
      // X-
      if(cell.neighbour(0)->state() == Cell::liquid) {
        numNeighbours++;
        m_pressureCoefficients.push_back(Triplet(cell.neighbour(0)->liquidIndex(), cell.liquidIndex(), -scale)); }
      else if(cell.neighbour(0)->state() == Cell::air) {
        numNeighbours++;
      }

      // Y-
      if(cell.neighbour(1)->state() == Cell::liquid) {
        numNeighbours++;
        m_pressureCoefficients.push_back(Triplet(cell.neighbour(1)->liquidIndex(), cell.liquidIndex(), -scale)); }
      else if(cell.neighbour(1)->state() == Cell::air) {
        numNeighbours++;
      }

      // Z-
      if(cell.neighbour(2)->state() == Cell::liquid) {
        numNeighbours++;
        m_pressureCoefficients.push_back(Triplet(cell.neighbour(2)->liquidIndex(), cell.liquidIndex(), -scale)); }
      else if(cell.neighbour(2)->state() == Cell::air) {
        numNeighbours++;
      }
      // X+
      if(cell.neighbour(3)->state() != Cell::solid) {
        numNeighbours++; }

      // Y+
      if(cell.neighbour(4)->state() != Cell::solid) {
        numNeighbours++; }

      // Z+
      if(cell.neighbour(5)->state() != Cell::solid) {
        numNeighbours++; }

      // Store diagonal value
      m_pressureCoefficients.push_back(Triplet(cell.liquidIndex(), cell.liquidIndex(), scale * numNeighbours));

    } // end if liquid
  } // end for cells

  // create a matrix from all the coefficients
  m_A = SpMat(numLiquidCells(), numLiquidCells());
  m_A.setFromTriplets(m_pressureCoefficients.begin(), m_pressureCoefficients.end());

  // solve linear system
  m_cg.compute(m_A);
  m_x = Eigen::VectorXd(numLiquidCells());
  m_x = m_cg.solve(m_b);

  // copy pressure values to the grid cells
  #pragma omp parallel for
  for(int i = 0; i < numCells(); ++i)
  {
    Cell& cell = m_cells[i];

    if(cell.state() == Cell::liquid) // liquid
    {
      cell.setP(m_x.coeff(cell.liquidIndex(), 0));
    }
    else if(cell.state() == Cell::air) // air
    {
      // setting this to 0 or 0.0 gives a memory free() error upon program exit(???)
      cell.setP(-0.0);
    }
    else // solid
    {
      /*
      If this was a solid OBSTACLE inside the fluid, we would not explicitly store
      the solid pressure, since we could potentially use multiple different values
      when calculating u,v,w pressure differences.
      We set boundary velocities directly anyway, instead of working them out
      through a pressure update. (Bridson 2016, p.70-71).

      That being said, we will set the solid cell pressure to its non-solid neighbour
      pressure, as Foster & Metaxas explain in their 1996 paper (page 9):
      http://www.cbim.rutgers.edu/dmdocuments/gmip96%20Foster.pdf
      */
      //cell.setP(cell.neighbour(0)->p());
    }
  }

  // debug

  //printCoeffMatrix(m_A, m_x);
  /*
  std::cout<<"Pressures: ";
  for(int i = 0; i < currCount; ++i)
  {
    std::cout<<m_x.coeff(i, 0)<<" ";
  }
  std::cout<<std::endl;

  std::cout<<"Iterations: "<<m_cg.iterations()<<std::endl;
  std::cout<<"Max iterations: "<<m_cg.maxIterations()<<std::endl;
  std::cout<<"Estimated error: "<<m_cg.error()<<std::endl;
  std::cout<<"-----------"<<std::endl;*/

}

//-----------------------------------Grid::pressureUpdate()------------------------
void Grid::pressureUpdate()
{
  /******** BRIEF **********
  Use the pressures we just calculated in computePressures() to enforce
  incompressibility and boundary conditions in the vector field.
  Only velocities that border liquid cells and no solid cells are
  updted with the pressure values.

  vel(new) = vel(current) - dt * 1/density * grad(pressure)
                           |
                           V
  vel(new) = vel(current) - dt/(density*dx) * (pressure_difference)
                           |
                           V
  u(new) = u(current) - (dt/(density * dx)) * ( (p(i,j,k) - p(i-1,j,k)) )
  v(new) = v(current) - (dt/(density * dx)) * ( (p(i,j,k) - p(i,j-1,k)) )
  w(new) = w(current) - (dt/(density * dx)) * ( (p(i,j,k) - p(i,j,k-1)) )

  Bridson, 2016: pages 69-71, Eq.(5.2), Figure(5.2)
  *************************/

  double scale = m_dt / (m_density * m_cellSize);
  #pragma omp parallel for
  for(int i = 0; i < numCells(); ++i)
  {
    Cell& cell = m_cells[i];

    if(cell.state() != Cell::solid)
    {
      // u
      if(cell.state() == Cell::liquid || cell.neighbour(0)->state() == Cell::liquid)
      {
        // velocity is between a liquid and a solid
        if(cell.neighbour(0)->state() == Cell::solid) { cell.setU(0); }

        // velocity is between 2 liquid cells or 1 liquid cell and 1 air cell
        else { cell.setU(cell.u() - scale * (cell.p() - cell.neighbour(0)->p())); }
      }

      // v
      if(cell.state() == Cell::liquid || cell.neighbour(1)->state() == Cell::liquid)
      {
        // velocity is between a liquid and a solid
        if(cell.neighbour(1)->state() == Cell::solid) { cell.setV(0); }

        // velocity is between 2 liquid cells or 1 liquid cell and 1 air cell
        else { cell.setV(cell.v() - scale * (cell.p() - cell.neighbour(1)->p())); }
      }

      // w
      if(cell.state() == Cell::liquid || cell.neighbour(2)->state() == Cell::liquid)
      {
        // velocity is between a liquid and a solid
        if(cell.neighbour(2)->state() == Cell::solid) { cell.setW(0); }

        // velocity is between 2 liquid cells or 1 liquid cell and 1 air cell
        else { cell.setW(cell.w() - scale * (cell.p() - cell.neighbour(2)->p())); }
      }

    } // end if !solid
  } // end for

  // set boundary velocities (just in case)
  setBoundaryVelocities();
}

//-----------------------------------Grid::project()-------------------------------
void Grid::project()
{
  /******** BRIEF **********
  A routine that makes the fluid incompressible by subtracting the pressure gradient
  from the velocity field in order to make it divergence-free and to enforce boundary
  conditions.
  A concise outline of the routine is given in Bridson, 2016, p.90, while all the
  details are discussed in Chapter 5, starting at p.67.
  *************************/

  // set boundary velocities (just in case, should be done prior to project())
  setBoundaryVelocities();

  // calculate the negative divergence b (the velocity field with modifications
  // at the solid wall boundaries)
  computeNegativeDivergence();

  // set the entries of matrix A (the matrix of pressure coefficients), solve for pressure
  computePressures();

  // pressure update
  pressureUpdate();
}

//-----------------------------------Grid::printCoeffMatrix()----------------------
void Grid::printCoeffMatrix(const Eigen::SparseMatrix<double>& _matrix, const Eigen::VectorXd& _vector) const
{
  std::cout<<"------------------START--------------------"<<std::endl;
  std::cout<<"A("<<_matrix.rows()<<","<<_matrix.cols()<<")"<<std::endl;
  std::cout<<"b("<<_matrix.rows()<<","<<_matrix.cols()<<")"<<std::endl;
  for(int i = 0; i < _matrix.rows(); ++i)
  {
    for(int j = 0; j < _matrix.rows(); ++j)
    {
      // print matrix coefficient
      double coeff = _matrix.coeff(i, j);
      if(coeff == 0) { std::cout<<"   0.00 "; continue; }
      if(coeff > 0) { std::cout<<" "; }
      if(coeff < 10) { std::cout<<"  "; }
      else if(coeff < 100) { std::cout<<" "; }
      std::cout<<coeff<<" ";
    }

    // print m_b elements
    std::cout<<"|"<<_vector.coeff(i, 0)<<"|";

    std::cout<<std::endl;
  }
  std::cout<<"-------------------END---------------------"<<std::endl;
}

//-----------------------------------Grid::numLiquidCells()------------------------
int Grid::numLiquidCells() const
{
  // loop through all the cells to get total number of liquid cells
  int count = 0;
  #pragma omp parallel for reduction(+:count)
  for(int i = 0; i < numCells(); ++i)
  {
    const Cell& cell = m_cells[i];
    if(cell.state() == Cell::liquid) { count++; }
  }

  return count;
}

//-----------------------------------Grid::computeMaxVelocity()--------------------
void Grid::computeMaxVelocity()
{
  /******** BRIEF **********
  Loops over all the cells and stores the absolute value of the
  maximum encountered velocity in any direction. We need this in calculateTimestep().
  *************************/
  // reset the max velocity first
  m_maxVelocity = 0.0f;

  // loop through all the cells and get the maximum velocity on the grid
  for(Cell& cell : m_cells)
  {
    if(abs(cell.u()) > m_maxVelocity) { m_maxVelocity = abs(cell.u()); }
    if(abs(cell.v()) > m_maxVelocity) { m_maxVelocity = abs(cell.v()); }
    if(abs(cell.w()) > m_maxVelocity) { m_maxVelocity = abs(cell.w()); }
  }

}

//-----------------------------------Grid::computeTimestep()-----------------------
void Grid::computeTimestep(float _k)
{
  /******** BRIEF **********
  Computes a timestep duration for the next iteration.
  From Foster and Fedkiw, 2001: dt <= (k * cellSize)/maxVelocity
  From Bridson, 2009: maxVelocity(n) = maxVelocity + sqrt(k * cellSize * abs(g))
  *************************/
  float maxVel = m_maxVelocity + sqrt(_k * m_cellSize * abs(m_gravity));

  // calculate timestep
  m_dt = (_k * m_cellSize) / maxVel;

  // if m_dt too big, clamp it down to the duration of the frame
  if(m_dt > m_frameDuration) { m_dt = m_frameDuration; }
}

//**********************************************************************************
//*********************************** OpenGL ***************************************
//**********************************************************************************

//-----------------------------------Grid::oglGrid()-------------------------------
std::unordered_map<std::string, ShapeData> Grid::oglGrid()
{
  // create X, Y, Z planes
  std::unordered_map<std::string, ShapeData> planes;

  ShapeFactory factory;
  planes["planeX"] = factory.makePlane("planeX",
                                   ShapeFactory::yz, m_ny * m_cellSize, m_nz  * m_cellSize);
  planes["planeY"] = factory.makePlane("planeY",
                                   ShapeFactory::xz, m_nx  * m_cellSize, m_nz  * m_cellSize);
  planes["planeZ"] = factory.makePlane("planeZ",
                                   ShapeFactory::yx, m_nx  * m_cellSize, m_ny * m_cellSize);
  return planes;
}

//-----------------------------------Grid::oglCell()-------------------------------
ShapeData Grid::oglCell(int _x, int _y, int _z, const glm::vec3& _color)
{
  // cell number based on input coords - (0,0,0) is bottom-left cell
  int cellNumber = cellIndex(_x, _y, _z);
  ShapeData cell = ShapeFactory().makeCell("cell" + std::to_string(cellNumber), m_cellSize);

  // position the cell
  glm::vec3 cellPosition(_x * m_cellSize, _y * m_cellSize, _z * m_cellSize);
  for(GLuint i = 0; i < cell.numVertices(); ++i)
  {
    cell.vertex(i).addPosition(cellPosition);
    cell.vertex(i).setColor(_color);
  }

  return cell;
}

//-----------------------------------Grid::oglFluidCells()-------------------------
ShapeData Grid::oglFluidCells(const glm::vec3& _color)
{
  ShapeData cells;
  float size = m_cellSize * 0.5f;

  // vertex & index data
  std::vector<Vertex>vertices;
  std::vector<GLushort>indices;

  // loop over all liquid cells
  int runner = 0;
  glm::vec3 pos;
  float r = _color.r; float g = _color.g; float b = _color.b;
  for(Cell& cell : m_cells)
  {
    if(cell.state() == Cell::liquid)
    {
      int offset = runner * 8;

      // set vertices
      pos = cell.position();
      vertices.push_back(Vertex(pos.x-size, pos.y+size, pos.z+size, r, g, b));
      vertices.push_back(Vertex(pos.x-size, pos.y-size, pos.z+size, r, g, b));
      vertices.push_back(Vertex(pos.x+size, pos.y-size, pos.z+size, r, g, b));
      vertices.push_back(Vertex(pos.x+size, pos.y+size, pos.z+size, r, g, b));
      vertices.push_back(Vertex(pos.x-size, pos.y-size, pos.z-size, r, g, b));
      vertices.push_back(Vertex(pos.x+size, pos.y-size, pos.z-size, r, g, b));
      vertices.push_back(Vertex(pos.x+size, pos.y+size, pos.z-size, r, g, b));
      vertices.push_back(Vertex(pos.x-size, pos.y+size, pos.z-size, r, g, b));

      // set indices
      // Z+ face
      indices.push_back(offset + 0); indices.push_back(offset + 1); // 0,1
      indices.push_back(offset + 1); indices.push_back(offset + 2); // 1,2
      indices.push_back(offset + 2); indices.push_back(offset + 3); // 2,3
      indices.push_back(offset + 3); indices.push_back(offset + 0); // 3,0

      // Z- face
      indices.push_back(offset + 7); indices.push_back(offset + 4); // 7,4
      indices.push_back(offset + 4); indices.push_back(offset + 5); // 4,5
      indices.push_back(offset + 5); indices.push_back(offset + 6); // 5,6
      indices.push_back(offset + 6); indices.push_back(offset + 7); // 6,7

      // connection lines in Z
      indices.push_back(offset + 0); indices.push_back(offset + 7); // 0,7
      indices.push_back(offset + 1); indices.push_back(offset + 4); // 1,4
      indices.push_back(offset + 2); indices.push_back(offset + 5); // 2,5
      indices.push_back(offset + 3); indices.push_back(offset + 6); // 3,6

      // increment runner
      runner++;
    }
  }

  //copy data to the shape
  cells.setNumVertices(vertices.size());
  cells.liveVertices().reset(new Vertex[vertices.size()]);
  for(int i = 0; i < (int)vertices.size(); ++i)
  {
    cells.vertex(i) = vertices[i];
  }

  cells.setNumIndices(indices.size());
  cells.liveIndices().reset(new GLushort[indices.size()]);
  for(int i = 0; i < (int)indices.size(); ++i)
  {
    cells.index(i) = indices[i];
  }

  return cells;
}

//---------------------------Grid::oglVelocityField()-------------------------------
ShapeData Grid::oglVelocityField(float _scale,
                                 const glm::vec3& _colorBase,
                                 const glm::vec3& _colorTip)
{
  ShapeData shape;

  // initialize some shape data
  int numCells = m_cells.size();
  shape.setNumVertices(numCells * 2);
  shape.liveVertices().reset(new Vertex[numCells * 2]);

  // iterate over vertices, set position and color
  #pragma omp parallel for
  for(int i = 0; i < numCells; ++i)
  {

    const Cell* currCell = cell(i);

    // average velocity
    float vx = (currCell->u() + currCell->uplus()) / 2.0f;
    float vy = (currCell->v() + currCell->vplus()) / 2.0f;
    float vz = (currCell->w() + currCell->wplus()) / 2.0f;
    glm::vec3 cellVelocity(vx, vy, vz);

    // base point (0,2,4...)
    Vertex& baseVert = shape.vertex(i * 2);
    baseVert.setPosition(currCell->position());
    baseVert.setColor(_colorBase);

    // tip point (1,3,5...)
    Vertex& tipVert = shape.vertex(i * 2 + 1);
    tipVert.setPosition(currCell->position() + (cellVelocity/(float)glm::length(cellVelocity)) * m_cellSize * _scale);
    tipVert.setColor(_colorTip);
  }

  return shape;
}

//---------------------------Grid::oglInterpolatedVelocityField()------------
ShapeData Grid::oglInterpolatedVelocityField(const glm::vec3& _startPoint,
                                             const glm::vec3& _endPoint,
                                             int _density, float _scale,
                                             const glm::vec3& _colorBase,
                                             const glm::vec3& _colorTip)
{
  ShapeData shape;

  // Calculate distance between start and end point and divide by density.
  // This gives the number of x, y and z SEGMENTS on each side of the area
  // we want to interpolate, stored as a single one vector.
  // To get the number of VECTORS, we need to add 1 to the number of segments.
  glm::vec3 diff = (_endPoint - _startPoint);
  shape.setNumVertices(2 * (_density * _density * _density));
  shape.liveVertices().reset(new Vertex[shape.numVertices()]);

  // create interpolated vectors
  glm::vec3 interp;
  int index = 0;

  for(float i = _startPoint.x; i <= _endPoint.x; i += diff.x / (_density-1))
  {
    for(float j = _startPoint.y; j <= _endPoint.y; j += diff.y / (_density-1))
    {
      for(float k = _startPoint.z; k <= _endPoint.z; k += diff.z / (_density-1))
      {
        // get interpolated velocity
        interp = interpolateVelocity(i, j, k);

        // base point
        Vertex& baseVert = shape.vertex(index);
        baseVert.setPosition(i, j, k);
        baseVert.setColor(_colorBase);

        // tip point
        Vertex& tipVert = shape.vertex(1 + index);
        tipVert.setPosition(baseVert.position() + interp/(float)glm::length(interp) * _scale);
        tipVert.setColor(_colorTip);

        // advance index
        index += 2;
      }
    }
  }
  return shape;
}

//---------------------------Grid::oglVector()-------------------------------
ShapeData Grid::oglVector(float _x, float _y, float _z,
                          const glm::vec3& _colorBase,
                          const glm::vec3& _colorTip)
{
  ShapeData shape;

  // initialize some shape data
  shape.setNumVertices(2);
  shape.liveVertices().reset(new Vertex[2]);

  // compute velocity vector at specified coordinates
  glm::vec3 v = interpolateVelocity(_x, _y, _z);

  // base point
  Vertex& baseVert = shape.vertex(0);
  baseVert.setPosition(_x, _y, _z);
  baseVert.setColor(_colorBase);

  // tip point
  Vertex& tipVert = shape.vertex(1);
  tipVert.setPosition(baseVert.position() + v/(float)glm::length(v) * 0.5f);
  tipVert.setColor(_colorTip);

  return shape;
}
