#include <integrator.h>
#include <grid.h>

glm::vec3 Euler::integrate(const glm::vec3& _pos, const glm::vec3& _vel, float _dt)
{
  // Euler's method explained on Khan Academy:
  // http://bit.ly/1bd3XLj

  return _pos + m_sign * (_vel * _dt);
}

glm::vec3 RK2::integrate(const glm::vec3& _pos, const glm::vec3& _vel, float _dt)
{  
  glm::vec3 pos = _pos + m_sign * (_vel * (_dt / 2.0f));
  glm::vec3 k = m_sign * m_grid->interpolateVelocity(pos.x, pos.y, pos.z); // k2
  return _pos + k * _dt;
}

glm::vec3 RK3::integrate(const glm::vec3& _pos, const glm::vec3& _vel, float _dt)
{
  // (dt*2/9)*k1 + (dt*3/9)*k2 + (dt*4/9)k3 - Ralston, 1962 (Bridson 2016, p. 242)

  glm::vec3 pos; // position
  glm::vec3 k; // velocity (from the interpolated vector field)
  glm::vec3 avgK; // weighted average velocity (2*k1 + 3*k2 + 4*k3)/9


  k = m_sign * _vel; // k1
  avgK += 2.0f * k;
  pos = _pos + k * (_dt / 2.0f);

  k = m_sign * m_grid->interpolateVelocity(pos.x, pos.y, pos.z); // k2
  avgK += 3.0f * k;
  pos = _pos + k * (3 * _dt / 4.0f);

  k = m_sign * m_grid->interpolateVelocity(pos.x, pos.y, pos.z); // k3
  avgK += 4.0f * k;

  avgK /= 9.0f;
  return _pos + avgK * _dt;
}

glm::vec3 RK4::integrate(const glm::vec3& _pos, const glm::vec3& _vel, float _dt)
{
  // RK4 explained nicely (with graph):
  // https://www.youtube.com/watch?v=1YZnic1Ug9g

  glm::vec3 pos; // position
  glm::vec3 k; // velocity (from the interpolated vector field)
  glm::vec3 avgK; // weighted average velocity (k1 + 2*k2 + 2*k3 + k4)/6

  k = m_sign * _vel; // k1
  avgK += k;
  pos = _pos + k * (_dt / 2.0f);

  k = m_sign * m_grid->interpolateVelocity(pos.x, pos.y, pos.z); // k2
  avgK += 2.0f * k;
  pos = _pos + k * (_dt / 2.0f);

  k = m_sign * m_grid->interpolateVelocity(pos.x, pos.y, pos.z); // k3
  avgK += 2.0f * k;
  pos = _pos + k * _dt;

  k = m_sign * m_grid->interpolateVelocity(pos.x, pos.y, pos.z); // k4
  avgK += k;

  avgK /= 6.0f;
  return _pos + avgK * _dt;
}
