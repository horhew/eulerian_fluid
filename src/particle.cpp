#include <particle.h>

//-------------------------------ctors/dtors------------------------------------------
Particle::Particle()
{
  m_position.x = 0; m_position.y = 0; m_position.z = 0;
  m_velocity.x = 0; m_velocity.y = 0; m_velocity.z = 0;
}

Particle::Particle(const glm::vec3& _position, const glm::vec3& _velocity,
                   Emitter* _emitter)
{
  m_position = _position;
  m_initPosition = _position;
  m_velocity = _velocity;
  m_initVelocity = _velocity;
  m_emitter = _emitter;
}

//-------------------------------kinematics-------------------------------------------
void Particle::update(const glm::vec3& _force, float _dt)
{
  // increment position and velocity
  if (!isFixed()) {
    m_position += m_velocity * _dt;
    m_velocity += _force * _dt;
  }

  // increment life
  m_currentLife += _dt * (1.0f/1000); // in milliseconds
}
