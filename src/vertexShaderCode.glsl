#version 400 core

// we ALWAYS get a vec4 regardless of what we pass (vec2/vec3/vec4) anyway
// 1.0 is used as the 4th component by default
in layout(location = 0) vec4 vertexPositionModel;
in layout(location = 1) vec3 vertexColor;
in layout(location = 2) vec3 normalModel;

// uniform vec3 lightPositionWorld; // passed directly to the fragment shader
uniform mat4 modelToProjectionMatrix;
uniform mat4 modelToWorldMatrix;

out vec3 normalWorld;
out vec3 vertexPositionWorld;
out vec3 vertColor;

void main()
{
  gl_Position = modelToProjectionMatrix * vertexPositionModel;

  //vertColor = vec3(brightness, brightness, brightness);
  // we put 0 in the vec4, so that translation doesn't affect the normals
  // ...also, we pass to the fragment shader an un-normalized vector
  // ...(!) make sure this vector is normalized in the fragment shader
  // ...before taking the dot product
  normalWorld = vec3(modelToWorldMatrix * vec4(normalModel, 0.0f));
  vertexPositionWorld = vec3(modelToWorldMatrix * vertexPositionModel);

  vertColor = vertexColor;
}
