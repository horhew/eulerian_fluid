#include <shapeData.h>

//**********************************************************************************
//********************************* ShapeData **************************************
//**********************************************************************************
ShapeData::ShapeData()
{

}

//----------------------------------ShapeData::data()----------------------------

ShapeData ShapeData::data()
{
    ShapeData data(m_name, m_type, m_numVertices, m_numIndices);

    // deep copy the unique ptrs
    // 1. vertices
    data.liveVertices().reset(new Vertex[m_numVertices]);
    for(GLuint i = 0; i < m_numVertices; i++)
    {
      data.vertex(i).setPosition(this->vertex(i).position());
      data.vertex(i).setColor(this->vertex(i).color());
      data.vertex(i).setNormal(this->vertex(i).normal());
    }

    // 2. indices
    data.liveIndices().reset(new GLushort[m_numIndices]);
    for(GLuint i = 0; i < m_numIndices; i++)
    {
      data.index(i) = m_indices[i];
    }

    // 3. uvs
    data.liveUvs().reset(new GLfloat[m_numVertices * 2]);
    for(GLuint i = 0; i < m_numVertices * 2; i+=2)
    {
      data.uv(i) = m_uvs[i]; // 0, 2, 4, 6 ...
      data.uv(1+i) = m_uvs[1+i]; // 1, 3, 5, 7 ...
    }

    return data;
}

//----------------------------------ShapeData::storeTo()----------------------------
void ShapeData::storeTo(std::vector<ShapeData*>& _toVec)
{
  _toVec.push_back(this);
}
