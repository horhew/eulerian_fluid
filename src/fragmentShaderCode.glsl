#version 400 core

out vec4 fragmentColor; // output
in vec3 normalWorld;
in vec3 vertexPositionWorld;
in vec3 vertColor;

uniform vec3 lightPositionWorld; // we can just pass in the uniform (bypass vertex shader)
uniform vec4 ambientLight; // same for ambient light (bypass vertex shader)
uniform vec3 eyePositionWorld;

void main()
{
  vec3 lightVectorWorld = normalize(lightPositionWorld - vertexPositionWorld);
  float brightness = vec3(1,1,1);

  // diffuse
  vec4 diffuseLight = vec4(brightness, brightness, brightness, 1.0);

  // specular (don't forget to negate the light vector)
  vec3 reflectedLightVectorWorld = reflect(-lightVectorWorld, normalWorld);
  vec3 eyeVectorWorld = normalize(eyePositionWorld - vertexPositionWorld);
  float spec = dot(reflectedLightVectorWorld, eyeVectorWorld);
  spec = pow(spec, 256);
  vec4 specularLight = vec4(spec, spec, spec, 1);

  // we clamp the diffuse because when surfaces are facing the other way from
  // the light, we get negative values and that subtracts from the ambient light
  fragmentColor = vec4(vertColor, 1);
  //fragmentColor = clamp(diffuseLight, 0, 1);
}
