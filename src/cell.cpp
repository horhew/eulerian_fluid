#include <grid.h>
#include <cell.h>

//**********************************************************************************
//********************************* Cell *******************************************
//**********************************************************************************

//-----------------------------------Cell ctors-------------------------------------
Cell::~Cell()
{

}

//---------------------------Cell::oglVelocityVectors()-----------------------------
ShapeData Cell::oglVelocityVectors(float _scale, bool _x, bool _y, bool _z) const
{
  ShapeData shape;

  // check which axes to render
  int numVerts = 0;
  _x ? numVerts+=2 : numVerts;
  _y ? numVerts+=2 : numVerts;
  _z ? numVerts+=2 : numVerts;

  // initialize some shape data
  shape.setNumVertices(numVerts);
  shape.liveVertices().reset(new Vertex[numVerts]);

  int i = 0;
  // u
  if(_x)
  {
    shape.vertex(i).setPosition(m_position.x - (m_size / 2.0f), m_position.y, m_position.z);
    shape.vertex(i).setColor(glm::vec3(1,0,0)); // red
    shape.vertex(i+1).setPosition(shape.vertex(i).position().x + u()*_scale, m_position.y, m_position.z);
    shape.vertex(i+1).setColor(glm::vec3(1,1,1));
    i+=2;
  }

  // v
  if(_y)
  {
    shape.vertex(i).setPosition(m_position.x, m_position.y - (m_size / 2.0f), m_position.z);
    shape.vertex(i).setColor(glm::vec3(0,1,0)); // green
    shape.vertex(i+1).setPosition(m_position.x, shape.vertex(i).position().y + v()*_scale, m_position.z);
    shape.vertex(i+1).setColor(glm::vec3(1,1,1));
    i+=2;
  }

  // w
  if(_z)
  {
    shape.vertex(i).setPosition(m_position.x, m_position.y, m_position.z - (m_size / 2.0f));
    shape.vertex(i).setColor(glm::vec3(0,0,1)); // blue
    shape.vertex(i+1).setPosition(m_position.x, m_position.y, shape.vertex(i).position().z + w()*_scale);
    shape.vertex(i+1).setColor(glm::vec3(1,1,1));
  }

  return shape;
}

//---------------------------Cell::setInterpNeighbours()--------------------------
void Cell::setInterpNeighbours()
{
  /******** BRIEF **********
  Stores pointers to the 7 neighbouring cells used in interpolation.
  Where a required neighbour does not exist, a nullptr is stored instead.
  ************************/
  // resize storage vectors
  m_interpNeighbours.resize(7);

  // index values of the neighbours within the m_cells array stored on grid
  std::vector<int> ni = {
    m_grid->cellIndex(m_i+1, m_j,   m_k),
    m_grid->cellIndex(m_i,   m_j+1, m_k),
    m_grid->cellIndex(m_i+1, m_j+1, m_k),
    m_grid->cellIndex(m_i,   m_j,   m_k+1),
    m_grid->cellIndex(m_i+1, m_j,   m_k+1),
    m_grid->cellIndex(m_i,   m_j+1, m_k+1),
    m_grid->cellIndex(m_i+1, m_j+1, m_k+1)
  };

  // right
  if(m_i+1 < m_grid->size(0))
  { m_interpNeighbours[0] = m_grid->cell(ni[0]); }
  else { m_interpNeighbours[0] = nullptr; }

  // above
  if(m_j+1 < m_grid->size(1))
  { m_interpNeighbours[1] = m_grid->cell(ni[1]); }
  else { m_interpNeighbours[1] = nullptr; }

  // right-above
  if(m_i+1 < m_grid->size(0) && m_j+1 < m_grid->size(1))
  { m_interpNeighbours[2] = m_grid->cell(ni[2]); }
  else { m_interpNeighbours[2] = nullptr; }

  // front
  if(m_k+1 < m_grid->size(2))
  { m_interpNeighbours[3] = m_grid->cell(ni[3]); }
  else { m_interpNeighbours[3] = nullptr; }

  // front-right
  if(m_i+1 < m_grid->size(0) && m_k+1 < m_grid->size(2))
  { m_interpNeighbours[4] = m_grid->cell(ni[4]); }
  else { m_interpNeighbours[4] = nullptr; }

  // front-above
  if(m_j+1 < m_grid->size(1) && m_k+1 < m_grid->size(2))
  { m_interpNeighbours[5] = m_grid->cell(ni[5]); }
  else { m_interpNeighbours[5] = nullptr; }

  // front-above-right
  if(m_i+1 < m_grid->size(0) && m_j+1 < m_grid->size(1) && m_k+1 < m_grid->size(2))
  { m_interpNeighbours[6] = m_grid->cell(ni[6]); }
  else { m_interpNeighbours[6] = nullptr; }

}

//---------------------------Cell::setNeighbours()----------------
void Cell::setNeighbours()
{
  /******** BRIEF **********
  Stores pointers to the neighbouring cells.
  ************************/

  if(state() != Cell::solid)
  {
    // resize storage vectors
    m_neighbours.resize(6);

    m_neighbours[0] = &m_grid->cell(m_i-1, m_j  , m_k  ); // xMin
    m_neighbours[1] = &m_grid->cell(m_i  , m_j-1, m_k  ); // yMin
    m_neighbours[2] = &m_grid->cell(m_i  , m_j  , m_k-1); // zMin
    m_neighbours[3] = &m_grid->cell(m_i+1, m_j  , m_k  ); // xMax
    m_neighbours[4] = &m_grid->cell(m_i  , m_j+1, m_k  ); // yMax
    m_neighbours[5] = &m_grid->cell(m_i  , m_j  , m_k+1); // zMax
  }
  else // solid
  {
    // we only store one neighbour: a non-solid cell
    m_neighbours.resize(1);

    /*
     -----------
     | |x|x|x| |  |x| store pointer to their fluid neighbour
     |x|     |x|  | | are grid corner cells, we don't care about them,
     |x|fluid|x|      since they don't actually border fluid.
     |x|     |x|
     | |x|x|x| |
     -----------
     Note: because of how we're setting this up, corner cells will store a pointer
     to a solid neighbour. Again, we don't care, since they don't border fluid.
    */


    if(m_i == 0) { // left wall
      m_neighbours[0] = &m_grid->cell(m_i+1, m_j, m_k);
    }
    else if(m_i == m_grid->size().x - 1) { // right wall
      m_neighbours[0] = &m_grid->cell(m_i-1, m_j, m_k);
    }

    if(m_j == 0) {// bottom wall
      m_neighbours[0] = &m_grid->cell(m_i, m_j+1, m_k);
    }
    else if(m_j == m_grid->size().y - 1) { // top wall
      m_neighbours[0] = &m_grid->cell(m_i, m_j-1, m_k);
    }

    if(m_k == 0) { // back wall
      m_neighbours[0] = &m_grid->cell(m_i, m_j, m_k+1);
    }
    else if(m_k == m_grid->size().z - 1) { // front wall
      m_neighbours[0] = &m_grid->cell(m_i, m_j, m_k-1);
    }

  }
}
