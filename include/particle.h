#ifndef PARTICLE_H
#define PARTICLE_H

#include <glm/vec3.hpp>
#include <unordered_map>
#include <vector>
#include <iostream>

class Emitter;

class Particle
{
public:
  // ctors & dtors
  Particle();
  Particle(const glm::vec3& _position, const glm::vec3& _velocity, Emitter* _emitter);

  // getters
  const glm::vec3&  position() const { return m_position; }
  const glm::vec3&  velocity() const { return m_velocity; }
  const glm::vec3&  initPosition() const { return m_initPosition; }
  const glm::vec3&  initVelocity() const { return m_initVelocity; }
  float             currentLife() const { return m_currentLife; }
  float             maxLife() const { return m_maxLife; }
  bool              isFixed() const { return m_fixed; }
  bool              isActive() const { return m_active; }
  const glm::vec3&  color() const { return m_color; }

  // setters
  void       setPosition(const glm::vec3& _position) { m_position = _position; }
  void       setVelocity(const glm::vec3& _velocity) { m_velocity = _velocity; }
  void       setCurrentLife(float _currentLife) { m_currentLife = _currentLife; }
  void       setMaxLife(float _maxLife) { m_maxLife = _maxLife; }
  void       setFixed(bool _fix) { m_fixed = _fix; }
  void       setActive() { m_active = true; }
  void       setInactive() { m_active = false; }
  void       setColor(const glm::vec3& _color) { m_color = _color; }

  // update
  void       update(const glm::vec3& _force, float _dt);

  // debug
  int        id() const { return m_id; }
  void       setId(int _id) { m_id = _id; }
private:
  glm::vec3  m_position;
  glm::vec3  m_velocity;
  glm::vec3  m_initPosition;
  glm::vec3  m_initVelocity;
  float      m_currentLife; // current life in milliseconds
  float      m_maxLife = 0; // max life in ms (0 = infinite)
  bool       m_active = true;
  bool       m_fixed = false;
  float      m_mass = 1; // attr. not used in practice. unit mass assumed.
  int        m_id;
  Emitter*   m_emitter;
  glm::vec3  m_color;
};

#endif // PARTICLE_H

