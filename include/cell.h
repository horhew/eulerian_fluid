#ifndef CELL_H
#define CELL_H

#include <vector>
#include <shapeFactory.h>
#include <glm/vec3.hpp>

class Grid;
//-----------------------------------Cell-------------------------------------------
class Cell
{
public:
  enum CellState {liquid, solid, air};
  enum Side {xMin, xMax, yMin, yMax, zMin, zMax};
  // ctors
  ~Cell();

  // getters
  double p() const { return *m_p; }
  float  u() const { return *m_u; }
  float  v() const { return *m_v; }
  float  w() const { return *m_w; }
  float  uplus() const { return *m_uplus; }
  float  vplus() const { return *m_vplus; }
  float  wplus() const { return *m_wplus; }
  double oldp() const { return m_oldp; }
  float  oldu() const { return m_oldu; }
  float  oldv() const { return m_oldv; }
  float  oldw() const { return m_oldw; }
  int    liquidIndex() const { return m_liquidIndex; }
  int    wavefront() const { return m_wavefront; }
  const  glm::vec3  velocity() const { return glm::vec3(*m_u, *m_v, *m_w); }
  const  glm::vec3& position() const { return m_position; }
  const  CellState& state() const { return m_cellState; }
  const  CellState& initialState() const { return m_cellState0; }
  const  Cell*      interpNeighbour(int _i) const { return m_interpNeighbours[_i]; }
  const  Cell*      neighbour(int _i) const { return m_neighbours[_i]; }
  std::vector<Cell*>& neighbours() { return m_neighbours; }
  bool              hasInterpNeighbour(int _i) const { return m_interpNeighbours[_i] != nullptr; }
  int  i() const { return m_i; }
  int  j() const { return m_j; }
  int  k() const { return m_k; }

  // setters
  void setPosition(const glm::vec3 _position) { m_position = _position; }
  void setState(CellState _state) { m_cellState = _state; }
  void setInitialState(CellState _initState) { m_cellState0 = _initState; }
  void setSize(float _size) { m_size = _size; }
  void setInterpNeighbours();
  void setNeighbours();
  void setCellCoords(int _i, int _j, int _k) { m_i = _i; m_j = _j; m_k = _k; }
  void setGrid(Grid* _grid) { m_grid = _grid; }
  void setP(double _p) { *m_p = _p; }
  void setU(float _u)  { *m_u = _u; }
  void setV(float _v)  { *m_v = _v; }
  void setW(float _w)  { *m_w = _w; }
  void setUplus(float _uplus) { *m_uplus = _uplus; }
  void setVplus(float _vplus) { *m_vplus = _vplus; }
  void setWplus(float _wplus) { *m_wplus = _wplus; }
  void setOldP(double _oldp) { m_oldp = _oldp; }
  void setOldU(float _oldu) { m_oldu = _oldu; }
  void setOldV(float _oldv) { m_oldv = _oldv; }
  void setOldW(float _oldw) { m_oldw = _oldw; }
  void setWavefront(int _wavefront) { m_wavefront = _wavefront; }
  void setLiquidIndex(int _liquidIndex) { m_liquidIndex = _liquidIndex; }
  void setPressurePointer(double* _p) { m_p = _p; }
  void setVelocityPointers(float* _u, float* _v, float* _w, float* _uplus, float* _vplus, float* _wplus) {
      m_u = _u; m_v = _v; m_w = _w; m_uplus = _uplus; m_vplus = _vplus; m_wplus = _wplus; }
  void reset();

  // openGL
  ShapeData oglVelocityVectors(float _scale = 1.0f,
                               bool _x = true, bool _y = true, bool _z = true) const;

private:
  // cell data
  float     m_size = 1.0f; // square cell, all sides are equal
  CellState m_cellState = air; // state
  CellState m_cellState0 = air; // initial state
  glm::vec3 m_position; // cell center position (where pressure is stored)
  int       m_i; // i index within the grid
  int       m_j; // j index within the grid
  int       m_k; // k index within the grid

  // pointers inside the arrays that are stored on the grid
  double*   m_p; // pressure
  float*    m_u; // xMin
  float*    m_v; // yMin
  float*    m_w; // zMin
  float*    m_uplus; // xMax
  float*    m_vplus; // yMax
  float*    m_wplus; // zMax

  // util data (used in various simulation steps)
  double    m_oldp; // old pressure
  float     m_oldu; // old u
  float     m_oldv; // old v
  float     m_oldw; // old w
  int       m_liquidIndex; // index within a (imaginary) liquid cell array
  int       m_wavefront = INT_MAX; // used in velocity extrapolation

  // grid relationships
  Grid* m_grid; // pointer to the grid
  std::vector<Cell*> m_interpNeighbours; // pointers to neighbours used in interpolation
  std::vector<Cell*> m_neighbours; // pointers to all 6 cell neighbours (1 in case of solid cells)
  void addNeighbour(Cell* _neighbour) { m_neighbours.push_back(_neighbour); }
};

#endif // CELL_H

