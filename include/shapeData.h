#ifndef SHAPEDATA
#define SHAPEDATA

#include <ngl/Types.h> // included just for glew and gl includes
#include <vector>
#include <memory>
#include <glm/vec3.hpp>
#include <vertex.h>

class ShapeData
{
public:
  // ctors
  ShapeData();
  ShapeData(std::string _name, std::string _type,
            GLuint _numVertices, GLuint _numIndices) :
             m_name(_name), m_type(_type),
             m_numVertices(_numVertices), m_numIndices(_numIndices) {}

  // getters
  std::string name() const { return m_name; }
  std::string type() const { return m_type; }
  GLuint      numVertices() const { return m_numVertices; }
  GLuint      numIndices() const { return m_numIndices; }
  GLsizeiptr  vertexBufferSize() const { return m_numVertices * sizeof(Vertex); }
  GLsizeiptr  indexBufferSize() const { return m_numIndices * sizeof(GLushort); }
  const std::unique_ptr<Vertex[]>&   vertices() const { return m_vertices; }
  const std::unique_ptr<GLushort[]>& indices() const { return m_indices; }
  const std::unique_ptr<GLfloat[]>&  uvs() const { return m_uvs; }

  // setters
  void        setName(std::string _name) { m_name = _name; }
  void        setType(std::string _type) { m_type = _type; }
  void        setNumVertices(GLuint _numVertices) { m_numVertices = _numVertices; }
  void        setNumIndices(GLuint _numIndices) { m_numIndices = _numIndices; }
  void        printVertices() const;

  // output
  ShapeData   data();
  void        storeTo(std::vector<ShapeData*>& _toVec);
  void        clear();

  // live references
  Vertex&     vertex(int _i) { return m_vertices.get()[_i]; }
  GLushort&   index(int _i) { return m_indices.get()[_i]; }
  GLfloat&    uv(int _i) { return m_uvs.get()[_i]; }
  std::unique_ptr<Vertex[]>& liveVertices() { return m_vertices; }
  std::unique_ptr<GLushort[]>& liveIndices() { return m_indices; }
  std::unique_ptr<GLfloat[]>& liveUvs() { return m_uvs; }

private:
  std::string m_name;
  std::string m_type;
  GLuint      m_numVertices;
  GLuint      m_numIndices;

  std::unique_ptr<Vertex[]> m_vertices;
  std::unique_ptr<GLushort[]> m_indices;
  std::unique_ptr<GLfloat[]> m_uvs;
};

#endif // SHAPEDATA

