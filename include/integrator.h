#ifndef INTEGRATOR_H
#define INTEGRATOR_H

#include <glm/vec3.hpp>

class Grid;

class Integrator // abstract base class
{
public:
  enum Type {euler, rk2, rk3, rk4};
  // ctors
  Integrator(Grid* _grid) : m_grid(_grid) {}

  // utils
  void setReverse(bool _reverse) { _reverse ? m_sign = -1.0f : m_sign = 1.0f; }
  virtual glm::vec3 integrate(const glm::vec3& _pos,
                              const glm::vec3& _vel,
                              float _dt) = 0;

protected:
  Grid* m_grid;
  float m_sign = 1.0f; // -1.0f when reverse integrating
  Type  m_type;
};

class Euler : public Integrator
{
public:
  // ctors
  Euler(Grid* _grid) : Integrator(_grid) { m_type = euler; }

  // integration
  glm::vec3 integrate(const glm::vec3& _pos,
                      const glm::vec3& _vel,
                      float _dt);
};

class RK2 : public Integrator
{
public:
  // ctors
  RK2(Grid* _grid) : Integrator(_grid) { m_type = rk2; }

  // integration
  glm::vec3 integrate(const glm::vec3& _pos,
                      const glm::vec3& _vel,
                      float _dt);
};

class RK3 : public Integrator
{
public:
  // ctors
  RK3(Grid* _grid) : Integrator(_grid) { m_type = rk3; }

  // integration
  glm::vec3 integrate(const glm::vec3& _pos,
                      const glm::vec3& _vel,
                      float _dt);
};

class RK4 : public Integrator
{
public:
  // ctors
  RK4(Grid* _grid) : Integrator(_grid) { m_type = rk4; }

  // integration
  glm::vec3 integrate(const glm::vec3& _pos,
                      const glm::vec3& _vel,
                      float _dt);
};

#endif // INTEGRATOR_H

