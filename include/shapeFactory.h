#ifndef SHAPEFACTORY_H
#define SHAPEFACTORY_H

#include <string>
#include <unordered_map>
#include <shapeData.h>


class ShapeFactory // mesh generator class
{
public:
  enum Plane {xy, yx, xz, zx, yz, zy};
  ShapeData makeCell(std::string _name, float _size);
  ShapeData makeSphere(std::string _name, float _radius);
  ShapeData makePlane(std::string _name, Plane _plane,
                      float _width, float _height);

  int  shapeCount() { return m_shapeCount; }
  void clearShapes() { m_shapeNames.clear(); m_shapeCount = 0; }
  std::vector<std::string> shapeNames() { return m_shapeNames; }

private:
  // utils
  void addShape(std::string _shapeName) { m_shapeNames.push_back(_shapeName); }
  glm::vec3 randomColor();

  int m_shapeCount = 0; // keeps track of how many geometry pieces were created
  std::vector<std::string> m_shapeNames; // keeps record of all created geometry names
};


#endif // SHAPEFACTORY_H
