#ifndef VERTEX_H
#define VERTEX_H

#include <glm/vec3.hpp>

class Vertex // Vertex (position, color, normal) class
{
public:
  Vertex();
  Vertex(glm::vec3 _position, glm::vec3 _color, glm::vec3 _normal) :
                m_position(_position),
                m_color(_color),
                m_normal(_normal) {}
  Vertex(float _x, float _y, float _z,
         float _r, float _g, float _b,
         float _nx = 0, float _ny = 0, float _nz = 0) :
         m_position(glm::vec3(_x, _y, _z)),
         m_color(glm::vec3(_r, _g, _b)),
         m_normal(glm::vec3(_nx, _ny, _nz)) {}
  ~Vertex();
  // getters and setters
  const glm::vec3& position() const { return m_position; }
  const glm::vec3& color() const { return m_color; }
  const glm::vec3& normal() const { return m_normal; }
  void addPosition(const glm::vec3& _position) { m_position += _position; }
  void setPosition(const glm::vec3& _position) { m_position = _position; }
  void setPosition(float _x, float _y, float _z) { m_position.x = _x;
                                                   m_position.y = _y;
                                                   m_position.z = _z;}
  void setColor(const glm::vec3& _color) { m_color = _color; }
  void setNormal(const glm::vec3& _normal) { m_normal = _normal; }
private:
  glm::vec3 m_position;
  glm::vec3 m_color;
  glm::vec3 m_normal;
};

#endif // VERTEX_H

