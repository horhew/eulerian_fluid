#ifndef EMITTER
#define EMITTER
#include <particle.h>
#include <shapeFactory.h>
#include <integrator.h>

class Grid;
class Emitter
{
public:
  enum Type {euler, rk2, rk3, rk4};
  Emitter() : m_position(glm::vec3(0, 0, 0)), m_numParticles(0) {}
  Emitter(Grid*            _grid,
          glm::vec3        _position,
          int              _numParticles,
          const glm::vec3& _spread,
          Type             _type);
  ~Emitter();

  // getters
  std::vector<Particle>& particles() { return m_particles; }
  std::vector<Particle>& newParticles() { return m_newParticles; }
  const glm::vec3& position() const { return m_position; }
  int              numParticles() const { return m_numParticles; }
  int              numNewParticles() const { return m_newParticles.size(); }
  float            ground() const { return m_ground; }
  bool             isGroundEnabled() const { return m_groundEnabled; }
  Type             integratorType() const { return m_integratorType; }

  // setters
  void setPosition(const glm::vec3& _pos) { m_position = _pos; }
  void setNumParticles(int _numParticles) { m_numParticles = _numParticles; }
  void setSpread(const glm::vec3& _spread) { m_spread = _spread; }
  void setGround(float _ground) { m_ground = _ground; }
  void enableGround() { m_groundEnabled = true; }
  void disableGround() { m_groundEnabled = false; }
  void update(float _dt);
  void emitParticles(int _pps, const glm::vec3& _spread = glm::vec3(1,1,1));
  void reset(); // reset particles to their initial state

  // openGL
  ShapeData oglParticles();
  ShapeData oglNewParticles(const glm::vec3& _color = glm::vec3(1,0,0), bool _randomColor = true);

private:
  glm::vec3              m_position; // emitter position
  int                    m_numParticles;
  int                    m_numNewParticles;
  std::vector<Particle>  m_particles;
  std::vector<Particle>  m_newParticles;
  glm::vec3              m_spread;
  float                  m_ground = 0.0f; // ground plane position
  bool                   m_groundEnabled = false;

  Grid*                  m_grid; // pointer to the grid
  std::unique_ptr<Integrator> m_integrator; // integrator
  Type                   m_integratorType; // integrator type
};

#endif // EMITTER

