#ifndef CAMERA_H
#define CAMERA_H
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

//----------------------------------------------------------------------------
// Class based on the tutorial series by Jamie King:
// https://www.youtube.com/watch?v=6c1QYZAEP2M&list=PLRwVmtr-pp06qT6ckboaOhnm9FxmzHpbY
//----------------------------------------------------------------------------

class Camera
{
public:
  Camera();
  void      mouseUpdate(const glm::vec2& _newMousePosition);
  void      setCameraSensitivity(float _sens) { m_rotationSpeed = _sens; }
  void      setCameraSpeed(float _speed) { m_movementSpeed = _speed; }
  glm::mat4 getWorldToViewMatrix() const;
  glm::vec3 position() const { return m_position; }
  glm::vec3 viewDirection() const { return m_viewDirection; }

    void    moveForward();
    void    moveBackward();
    void    strafeLeft();
    void    strafeRight();
    void    moveUp();
    void    moveDown();

    void    reset();
private:
  glm::vec3 m_viewDirection;
  glm::vec3 m_up;
  glm::vec3 m_position;
  glm::vec3 m_defaultPosition;
  glm::vec2 m_oldMousePosition;
  float     m_rotationSpeed;
  float     m_movementSpeed;
  glm::vec3 m_strafeDirection;
};

#endif // CAMERA_H
