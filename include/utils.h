#ifndef UTILS_H
#define UTILS_H

#ifndef THRESH
#define THRESH 0.002f
#endif

#include <iostream>
#include <glm/vec3.hpp>

inline float compare(float _a, float _b)
{
  if(_a > (_b - THRESH) && _a < (_b + THRESH))
  {
    return _b;
  }
  return _a;
}

inline void compareVec(glm::vec3& _a, glm::vec3& _b)
{
  if(_a.x > (_b.x - THRESH) && _a.x < (_b.x + THRESH)) { _a.x = 0; }
  if(_a.y > (_b.y - THRESH) && _a.y < (_b.y + THRESH)) { _a.y = 0; }
  if(_a.z > (_b.z - THRESH) && _a.z < (_b.z + THRESH)) { _a.z = 0; }
}

void printVec(const glm::vec3& _vec)
{
  std::cout<<"<"<<_vec.x<<", "<<_vec.y<<", "<<_vec.z<<">"<<std::endl;
}

#endif // UTILS_H

