#ifndef GRID_H
#define GRID_H

#include <string>
#include <memory>
#include <unordered_map>
#include <emitter.h>
#include <integrator.h>
#include <eigen3/Eigen/Sparse>
#include <cell.h>

typedef Eigen::SparseMatrix<double> SpMat;
typedef Eigen::Triplet<double> Triplet;
typedef Eigen::ConjugateGradient<Eigen::SparseMatrix<double>, Eigen::Upper> ConjugateGradient;

//-----------------------------------Grid-------------------------------------------
class Grid
{
public:
  enum Axis {x, y, z};
  // ctors
  Grid();
  Grid(int _x, int _y, int _z, float _cellSize);
  ~Grid();

  // getters
  Cell&       cell(int _i, int _j, int _k);
  Cell&       cellAtPosition(const glm::vec3& _pos);
  Cell*       cell(int _index) { return &m_cells[_index]; }
  const std::vector<Cell>& cells() const { return m_cells; }
  int         cellIndex(int _x, int _y, int _z) const;
  glm::vec3   interpolateVelocity(float _x, float _y, float _z);
  glm::vec3   interpolateVelocity(const glm::vec3& _pos) {
                    return interpolateVelocity(_pos.x, _pos.y, _pos.z); }
  std::vector<int> velocityIndices(int _x, int _y, int _z);
  Cell&       boundaryCell(int _i) { return m_cells[m_boundaryCellIndices[_i]]; }
  int         numBoundaryCells() const { return m_boundaryCellIndices.size(); }
  float       cellSize() const { return m_cellSize; }
  int         numCells() const { return m_nx * m_ny * m_nz; }
  int         numLiquidCells() const;
  glm::vec3   size() const { return glm::vec3(m_nx, m_ny, m_nz); }
  int         size(int _axis) const;
  bool        isAnimating() const { return m_animating; }
  float       time() const { return m_time; }
  float       timestep() const { return m_dt; }
  int&        timer() { return m_timer; }

  // setters
  void      extrapolateVelocity();
  void      setFrameDuration(float _frameDuration) { m_frameDuration = _frameDuration; }
  void      updateCellStates(bool _changeInit = false);
  void      clearAirVelocity();
  void      resetTime() { m_time = 0.0f; }
  void      startAnimating() { m_animating = true; }
  void      stopAnimating() { m_animating = false; }
  void      setBoundaryCells();
  void      setBoundarySlip(int _slip) { m_slip = _slip; } // slip can be 1 or -1
  void      reset();

  // fluid steps
  void      animate();
  void      advect();
  void      project();
  void      setBoundaryVelocities();
  void      applyGravity();
  void      computeMaxVelocity();
  void      computeNegativeDivergence();
  void      computePressures();
  void      pressureUpdate();
  void      computeTimestep(float _k = 1.0f);
  void      advanceTime() { m_time += m_dt; }

  // debug
  void      printCoeffMatrix(const Eigen::SparseMatrix<double>& _matrix, const Eigen::VectorXd& _vector) const;

  // particles
  Emitter&  emitter() { return *m_emitter; }
  void      createParticles(glm::vec3& _emitterPos,int _numParticles, glm::vec3& _spread);
  void      moveParticles() { m_emitter->update(m_dt); }

  // openGL
  std::unordered_map<std::string, ShapeData> oglGrid();
  ShapeData oglCell(int _x, int _y, int _z, const glm::vec3& _color = glm::vec3(1,0,0));
  ShapeData oglVelocityField(float _scale = 0.5f,
                             const glm::vec3& _colorBase = glm::vec3(0,0,1),
                             const glm::vec3& _colorTip = glm::vec3(1,1,1));
  ShapeData oglInterpolatedVelocityField(const glm::vec3& _startPoint, const glm::vec3& _endPoint,
                                         int _density = 2, float _scale = 1.0f,
                                         const glm::vec3& _colorBase = glm::vec3(1,0,0),
                                         const glm::vec3& _colorTip = glm::vec3(1,1,0));
  ShapeData oglVector(float _x, float _y, float _z,
                      const glm::vec3& _colorBase = glm::vec3(1,0,0),
                      const glm::vec3& _colorTip = glm::vec3(1,1,0));
  ShapeData oglFluidCells(const glm::vec3& _color = glm::vec3(1,0,1));

private:
  // grid info
  float m_cellSize;
  int   m_nx; // number of cells in X
  int   m_ny; // number of cells in Y
  int   m_nz; // number of cells in Z

  // boundary slip condition (1 = free-slip, -1 = no-slip)
  int   m_slip = 1;

  // data storage
  std::vector<Cell>   m_cells;
  std::vector<int>    m_boundaryCellIndices; // array indices of boundary cells
  std::vector<std::vector<int>> m_velocityIndices; // velocity indices for each cell (to be used into velocity arrays)
  std::vector<float>  m_xVelocities; // velocities along X walls ( | | | ... | )
  std::vector<float>  m_yVelocities; // velocities along Y walls ( _ _ _ ... _ )
  std::vector<float>  m_zVelocities; // velocities along Z walls ( □ □ □ ... □ )
  std::vector<double> m_pressures;   // cell pressures
  double              m_density = 1.0;  // fluid density

  // util data storage
  std::vector<Triplet> m_pressureCoefficients;  // triplet (i, j, value)
  ConjugateGradient    m_cg; // conjugate gradient linear solver
  SpMat                m_A; // row-major sparse matrix from Ax = b
  Eigen::VectorXd      m_x; // column vector to store solutions from Ax = b
  Eigen::VectorXd      m_b; // cell velocity divergences from Ax = b

  std::vector<float> m_xVelocities0; // init velocities along X walls ( | | | ... | )
  std::vector<float> m_yVelocities0; // init velocities along Y walls ( _ _ _ ... _ )
  std::vector<float> m_zVelocities0; // init velocities along Z walls ( □ □ □ ... □ )
  std::vector<double> m_pressures0;   // cell init pressures

  // particles
  std::unique_ptr<Emitter> m_emitter; // particle emitter
  float m_gravity = -9.8f;

  // time variables
  std::unique_ptr<Integrator> m_integrator; // integrator
  int   m_timer; // QTimer id
  float m_dt = 0.02f; // timestep (in s)
  float m_time = 0.0f; // current time (in s)
  float m_frameDuration; // frame duration (in s)
  float m_maxVelocity = 0.0f; // max velocity on the grid at the current timestep
  float m_maxVelocity0 = 0.0f; // init max velocity on the grid
  bool  m_animating = true; // is the grid animating
};

#endif // GRID_H
