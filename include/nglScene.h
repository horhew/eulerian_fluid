#ifndef NGLSCENE_H__
#define NGLSCENE_H__
#include <ngl/Vec3.h>
#include <QOpenGLWindow>
#include <QGLWidget>
#include <iostream>
#include <camera.h>
#include <grid.h>

class NGLScene : public QOpenGLWindow
{
public:
  NGLScene();
  ~NGLScene();

  void initializeGL(); // called just after OpenGL context is created
  void paintGL();
  void resizeGL(QResizeEvent* _event); // qt 5.5
  void resizeGL(int _w, int _h); // qt 5.4

  // data creation
  void createVbo(const ShapeData& _shape);
  void createVao(const ShapeData& _shape);
  void installShaders();

  // data drawing
  void draw(const ShapeData& _shape, // shape name
             const glm::mat4& _mM, // model matrix
             const glm::mat4& _vpM, // view-projection matrix
             const GLenum _drawMethod = GL_TRIANGLES, // draw method
             bool _indexing = true); // use vertex indexing
  void drawGrid(const Grid& _grid, // grid
                const glm::mat4& _vpM); // view-projection matrix

  // utils
  void bgColor(int _r, int _g, int _b, float _a = 1.0f);

private:
  // event methods
  void keyPressEvent(QKeyEvent* _event);
  void mouseMoveEvent (QMouseEvent* _event );
  void mousePressEvent ( QMouseEvent* _event);
  void mouseReleaseEvent ( QMouseEvent* _event );
  void wheelEvent( QWheelEvent* _event);
  void timerEvent(QTimerEvent*);

  // shader-related methods
  std::string readShaderCode(const char* _filename);
  void        checkStatus(const GLuint& _objID,
                          std::string   _objName = "Object",
                          bool          _isShader = true);

  // program id
  GLuint m_programID;

  // camera
  Camera m_camera;
  bool m_clicked = false;

  // debug viz
  bool m_ogl_grid = true;
  bool m_ogl_velocity_field = false;
  bool m_ogl_cell_wall_velocities = true;
  bool m_ogl_fluid_cells = true;
  bool m_ogl_interpolated_velocity = false;
  bool m_ogl_particles = true;

  // control
  int  m_emitRate = 20000; // particle emit rate (per second)

  // VBOs,VAOs,VSO,FSO id storage
  GLuint m_vbo; // shared vertex buffer id
  std::unordered_map<std::string, GLuint> m_vaos; // vertex array ids
  std::unordered_map<std::string, GLuint> m_indexOffset; // index data offsets (VAOs)

  GLuint m_vso; // vertex shader id
  GLuint m_fso; // fragment shader id
  std::unordered_map<std::string, GLint> m_uniformLocations; // shader uniform locations

  // Grid
  std::unique_ptr<Grid> m_grid;
  std::unordered_map<std::string, ShapeData> m_gridPlanes; // X,Y,Z grid planes
};

#endif