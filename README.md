## Eulerian fluid solver

OpenGL Eulerian fluid solver written in C++. This was my Masters' project and it was a tough challlenge. Overall, I could not bring the project to the level that I would have liked, but on the bright side I did learn many things about physically-based fluid simulation, which is a fascinating (and hard) subject. The solver is purely Eulerian, all the calculations happen on a staggered MAC grid and marker particles are used to track the fluid. This probably sounds like an easy task for those more versed in simulation or graphics programming in general, but it sucked a fair chunk of my commoner's soul during the 6-7 weeks of active development.

The primary source of documentation was Robert Bridson's "Fluid Simulation for Computer Graphics, Second Edition" (2015), a book which I found to be both intimidating and extremely helpful.

***

### External libraries

NGL, glm, Eigen, OpenMP

### Hotkeys

**_Camera:_**

WASD, R,F, mouse : move and pan camera

C : reset camera

**_Animation controls:_**

Z = pause/resume simulation

X = reset simulation

V = step through the simulation 1 timestep at a time (works from paused state)

B = emit particles into the scene (gets slow really fast for some reason that I have yet to figure out)
visualization controls (debug modes)

**_Debuggin visualization:_**

1 = toggle grid

2 = toggle velocity field

3 = toggle fluid cells

4 = toggle velocity components

5 = toggle interpolated velocity field (resolution can be changed in the draw call inside paintGL() )

6 = toggle particle